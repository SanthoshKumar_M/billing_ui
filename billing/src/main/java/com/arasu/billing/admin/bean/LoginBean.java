package com.arasu.billing.admin.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.admin.to.LoginTO;
import com.arasu.billing.master.controller.BranchController;
import com.arasu.billing.master.to.BranchMasterTO;

@Named
@ViewScoped
public class LoginBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private LoginTO loginTO = new LoginTO();
	private BranchMasterTO branchMasterTO;
	private List<BranchMasterTO> branchMasterTOs = new ArrayList<>();

	private BranchController branchController;

	@Inject
	public LoginBean(final BranchController branchController) {
		this.branchController = branchController;
	}

	public LoginBean() {
		
	}

	@PostConstruct
	public void init() {
		branchMasterTOs = branchController.getAllBranchs();
	}

	public LoginTO getLoginTO() {
		return loginTO;
	}

	public void setLoginTO(LoginTO loginTO) {
		this.loginTO = loginTO;
	}

	public BranchMasterTO getBranchMasterTO() {
		return branchMasterTO;
	}

	public void setBranchMasterTO(BranchMasterTO branchMasterTO) {
		this.branchMasterTO = branchMasterTO;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<BranchMasterTO> getBranchMasterTOs() {
		return branchMasterTOs;
	}

	public void setBranchMasterTOs(List<BranchMasterTO> branchMasterTOs) {
		this.branchMasterTOs = branchMasterTOs;
	}

}

package com.arasu.billing.admin.controller;

import java.io.IOException;
import java.util.Base64;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import com.arasu.billing.admin.bean.LoginBean;
import com.arasu.billing.admin.to.LoginTO;
import com.arasu.billing.common.CurrentInstance;
import com.arasu.billing.common.Messages;
import com.arasu.billing.common.UserSession;
import com.arasu.billing.service.HttpClientService;

@RequestScoped
@Named
public class LoginController {

	private LoginBean loginBean;
	private UserSession userSession;
	private HttpClientService httpClientService;
	private CurrentInstance currentInstance;
	private Messages messages;

	@Inject
	public LoginController(final LoginBean loginBean, final UserSession userSession, final HttpClientService httpClientService, final CurrentInstance currentInstance,
			final Messages messages) {
		this.loginBean = loginBean;
		this.userSession = userSession;
		this.httpClientService = httpClientService;
		this.currentInstance = currentInstance;
		this.messages = messages;
	}

	public boolean checkLoginDetails() {
		LoginTO loginTO = null;
		boolean result = false;
		String url = "getLoginUserDetails/" + loginBean.getLoginTO().getUserName() + "/" + loginBean.getLoginTO().getPassword();
		loginTO = httpClientService.get(url, LoginTO.class);
		if (null != loginTO && loginTO.getId() > 0) {
			HttpSession httpSession = currentInstance.getHttpSession();
			userSession.setLoginTO(loginTO);
			userSession.setBranchMasterTO(loginBean.getBranchMasterTO());
			if (null != loginTO.getEmployeeMaster() && null != loginTO.getEmployeeMaster().getEmployeePhoto()) {
				userSession.setEmployeePhoto(Base64.getEncoder().encodeToString(loginTO.getEmployeeMaster().getEmployeePhoto()));
			}
			httpSession.setAttribute("userobject", userSession);
			result = true;
		} else {
			messages.displayErrorMessage(null, "Error", "Login Credentials Wrong Contact Admin.");
		}
		return result;
	}

	public void logout() {
		HttpSession httpSession = currentInstance.getHttpSession();
		try {
			currentInstance.getCurrentInstance().getExternalContext().redirect("../../admin/login/login.xhtml");
		} catch (IOException e) {
			messages.displayErrorMessage(null, "Error", "Not Logout..!");
		} finally {
			httpSession.invalidate();
		}
	}

}

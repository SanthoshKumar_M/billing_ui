package com.arasu.billing.admin.service;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.base.service.BaseService;
import com.arasu.billing.service.HttpClientService;

@Named
@RequestScoped
public class DCRunningNumberService extends BaseService {

	public DCRunningNumberService() {

	}

	@Inject
	public DCRunningNumberService(final HttpClientService httpClientService) {
		super(httpClientService);
	}

	public int getCurrentDCRunningNumber(int branchMasterId) {
		return getHttpClientService().get("getCurrentDCRunningNumber/" + branchMasterId, Integer.class);
	}
}

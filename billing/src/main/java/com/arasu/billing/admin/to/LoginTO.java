package com.arasu.billing.admin.to;

import java.io.Serializable;

import com.arasu.billing.master.to.EmployeeMasterTO;

public class LoginTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private EmployeeMasterTO employeeMaster;
	private String userName;
	private String password;
	private boolean activeUser;

	public LoginTO() {
		this.activeUser = Boolean.TRUE;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public EmployeeMasterTO getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMasterTO employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isActiveUser() {
		return activeUser;
	}

	public void setActiveUser(boolean activeUser) {
		this.activeUser = activeUser;
	}

}

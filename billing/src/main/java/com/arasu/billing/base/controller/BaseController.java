package com.arasu.billing.base.controller;

import com.arasu.billing.common.Messages;
import com.arasu.billing.common.PrimeFacesUtil;

public class BaseController extends PrimeFacesUtil{

	private Messages messages;

	public BaseController(final Messages messages) {
		this.messages = messages;
	}

	public BaseController() {

	}

	public Messages getMessages() {
		return messages;
	}

	public void setMessages(Messages messages) {
		this.messages = messages;
	}

}

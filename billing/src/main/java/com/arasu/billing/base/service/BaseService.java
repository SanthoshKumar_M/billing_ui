package com.arasu.billing.base.service;

import com.arasu.billing.service.HttpClientService;

public class BaseService {

	private HttpClientService httpClientService;

	public BaseService(final HttpClientService httpClientService) {
		this.httpClientService = httpClientService;
	}

	public BaseService() {
		
	}

	public HttpClientService getHttpClientService() {
		return httpClientService;
	}

	public void setHttpClientService(HttpClientService httpClientService) {
		this.httpClientService = httpClientService;
	}

}

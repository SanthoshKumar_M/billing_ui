package com.arasu.billing.base.to;

import java.io.Serializable;
import java.util.Date;

import com.arasu.billing.master.to.EmployeeMasterTO;

public class BaseTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private int rowStatus;
	private EmployeeMasterTO createUser;
	private EmployeeMasterTO updateUser;
	private Date createDateTime;
	private Date updateDateTime;

	public BaseTO() {
		this.rowStatus = 1;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EmployeeMasterTO getCreateUser() {
		return createUser;
	}

	public void setCreateUser(EmployeeMasterTO createUser) {
		this.createUser = createUser;
	}

	public EmployeeMasterTO getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(EmployeeMasterTO updateUser) {
		this.updateUser = updateUser;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public int getRowStatus() {
		return rowStatus;
	}

	public void setRowStatus(int rowStatus) {
		this.rowStatus = rowStatus;
	}

}

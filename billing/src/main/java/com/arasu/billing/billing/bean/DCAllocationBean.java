package com.arasu.billing.billing.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.billing.to.DCListTO;
import com.arasu.billing.common.CurrentInstance;

@Named
@ViewScoped
public class DCAllocationBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private CurrentInstance currentInstance;

	private List<Integer> cityIds = new ArrayList<>();
	private List<Integer> branchIds = new ArrayList<>();

	private List<DCListTO> selectedDCIds = new ArrayList<>();
	private List<DCListTO> dcList = new ArrayList<>();
	private Date selectedDCDate;
	private int selectedDriverId;
	private int selectedVehicleId;
	private int scanDCId;

	@Inject
	public DCAllocationBean(final CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	public DCAllocationBean() {
		
	}

	@PostConstruct
	public void init() {
		selectedDCDate = new Date();
	}

	public CurrentInstance getCurrentInstance() {
		return currentInstance;
	}

	public void setCurrentInstance(CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	public List<Integer> getCityIds() {
		return cityIds;
	}

	public void setCityIds(List<Integer> cityIds) {
		this.cityIds = cityIds;
	}

	public List<DCListTO> getSelectedDCIds() {
		return selectedDCIds;
	}

	public void setSelectedDCIds(List<DCListTO> selectedDCIds) {
		this.selectedDCIds = selectedDCIds;
	}

	public int getSelectedDriverId() {
		return selectedDriverId;
	}

	public void setSelectedDriverId(int selectedDriverId) {
		this.selectedDriverId = selectedDriverId;
	}

	public int getSelectedVehicleId() {
		return selectedVehicleId;
	}

	public void setSelectedVehicleId(int selectedVehicleId) {
		this.selectedVehicleId = selectedVehicleId;
	}

	public Date getSelectedDCDate() {
		return selectedDCDate;
	}

	public void setSelectedDCDate(Date selectedDCDate) {
		this.selectedDCDate = selectedDCDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<DCListTO> getDcList() {
		return dcList;
	}

	public void setDcList(List<DCListTO> dcList) {
		this.dcList = dcList;
	}

	public List<Integer> getBranchIds() {
		return branchIds;
	}

	public void setBranchIds(List<Integer> branchIds) {
		this.branchIds = branchIds;
	}

	public int getScanDCId() {
		return scanDCId;
	}

	public void setScanDCId(int scanDCId) {
		this.scanDCId = scanDCId;
	}

}

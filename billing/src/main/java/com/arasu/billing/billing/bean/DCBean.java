package com.arasu.billing.billing.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.billing.to.DCLineTO;
import com.arasu.billing.billing.to.DCTO;
import com.arasu.billing.common.BillingConstant;
import com.arasu.billing.common.CurrentInstance;
import com.arasu.billing.common.PrimeFacesUtil;
import com.arasu.billing.common.paymentmode.PaymentMode;
import com.arasu.billing.data.DCData;
import com.arasu.billing.master.to.CityMasterTO;

@Named
@ViewScoped
public class DCBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private DCData dcData = new DCData();
	private Date maxDate;
	private CurrentInstance currentInstance;
	private int searchDCNo;
	private CityMasterTO fromCustomerCityMaster;
	private List<DCTO> dCList = new ArrayList<>();
	private transient PrimeFacesUtil primeFacesUtil;

	@Inject
	public DCBean(final CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	public DCBean() {
		
	}

	@PostConstruct
	public void init() {
		maxDate = new Date();
		dcData.getDc().setdCDate(maxDate);
		dcData.getDc().setBranchMaster(currentInstance.getLoginBranchDetails());
		dcData.getDc().setCreateUser(currentInstance.getEmployeeMaster());
		dcData.getDc().setModeOfPay(Integer.parseInt(
				PaymentMode.getModeOfPaymentList().stream().filter(pay -> pay.getLabel().equals(BillingConstant.TOPAY_PAYMENT_MODE)).findAny().get().getValue().toString()));
		addNewRowItemList();
	}

	public void addNewRowItemList() {
		dcData.getDcLine().add(new DCLineTO(currentInstance.getEmployeeMaster()));
	}

	public List<DCTO> getdCList() {
		return dCList;
	}

	public void setdCList(List<DCTO> dCList) {
		this.dCList = dCList;
	}

	public DCData getDcData() {
		return dcData;
	}

	public void setDcData(DCData dcData) {
		this.dcData = dcData;
	}

	public CurrentInstance getCurrentInstance() {
		return currentInstance;
	}

	public void setCurrentInstance(CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	public Date getMaxDate() {
		return maxDate;
	}

	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}

	public int getSearchDCNo() {
		return searchDCNo;
	}

	public void setSearchDCNo(int searchDCNo) {
		this.searchDCNo = searchDCNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CityMasterTO getFromCustomerCityMaster() {
		return fromCustomerCityMaster;
	}

	public void setFromCustomerCityMaster(CityMasterTO fromCustomerCityMaster) {
		this.fromCustomerCityMaster = fromCustomerCityMaster;
	}

	public PrimeFacesUtil getPrimeFacesUtil() {
		return primeFacesUtil;
	}

	public void setPrimeFacesUtil(PrimeFacesUtil primeFacesUtil) {
		this.primeFacesUtil = primeFacesUtil;
	}

}

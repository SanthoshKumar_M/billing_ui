package com.arasu.billing.billing.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.billing.to.DCListTO;
import com.arasu.billing.common.CurrentInstance;

@Named
@ViewScoped
public class DCCollectionBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private CurrentInstance currentInstance;

	private List<DCListTO> dcList = new ArrayList<>();
	private List<DCListTO> selectedDCIds = new ArrayList<>();

	private String selectedDriverId;
	
	private int scanDCId;
	
	private BigDecimal totalAmount = BigDecimal.valueOf(0.00);


	@Inject
	public DCCollectionBean(final CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	public DCCollectionBean() {
	
	}

	public CurrentInstance getCurrentInstance() {
		return currentInstance;
	}

	public void setCurrentInstance(CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	public List<DCListTO> getDcList() {
		return dcList;
	}

	public void setDcList(List<DCListTO> dcList) {
		this.dcList = dcList;
	}

	public List<DCListTO> getSelectedDCIds() {
		return selectedDCIds;
	}

	public void setSelectedDCIds(List<DCListTO> selectedDCIds) {
		this.selectedDCIds = selectedDCIds;
	}

	public String getSelectedDriverId() {
		return selectedDriverId;
	}

	public void setSelectedDriverId(String selectedDriverId) {
		this.selectedDriverId = selectedDriverId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public int getScanDCId() {
		return scanDCId;
	}

	public void setScanDCId(int scanDCId) {
		this.scanDCId = scanDCId;
	}	
	
}

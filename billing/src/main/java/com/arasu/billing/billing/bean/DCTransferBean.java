package com.arasu.billing.billing.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.billing.to.DCListTO;
import com.arasu.billing.common.CurrentInstance;

@Named
@ViewScoped
public class DCTransferBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private CurrentInstance currentInstance;

	private List<DCListTO> dcList = new ArrayList<>();
	private List<DCListTO> selectedDCIds = new ArrayList<>();

	private int selectedDriverId;
	private int transferDriverId;

	@Inject
	public DCTransferBean(final CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	public DCTransferBean() {
		
	}

	public CurrentInstance getCurrentInstance() {
		return currentInstance;
	}

	public void setCurrentInstance(CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	public List<DCListTO> getDcList() {
		return dcList;
	}

	public void setDcList(List<DCListTO> dcList) {
		this.dcList = dcList;
	}

	public List<DCListTO> getSelectedDCIds() {
		return selectedDCIds;
	}

	public void setSelectedDCIds(List<DCListTO> selectedDCIds) {
		this.selectedDCIds = selectedDCIds;
	}

	public int getSelectedDriverId() {
		return selectedDriverId;
	}

	public void setSelectedDriverId(int selectedDriverId) {
		this.selectedDriverId = selectedDriverId;
	}

	public int getTransferDriverId() {
		return transferDriverId;
	}

	public void setTransferDriverId(int transferDriverId) {
		this.transferDriverId = transferDriverId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}

package com.arasu.billing.billing.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.billing.to.DCListTO;
import com.arasu.billing.common.CurrentInstance;

@Named
@ViewScoped
public class OfficeItemBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private CurrentInstance currentInstance;

	private List<DCListTO> selectedDCIds = new ArrayList<>();
	private List<DCListTO> dcList = new ArrayList<>();

	@Inject
	public OfficeItemBean(final CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	public OfficeItemBean() {
		
	}

	public CurrentInstance getCurrentInstance() {
		return currentInstance;
	}

	public void setCurrentInstance(CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	public List<DCListTO> getSelectedDCIds() {
		return selectedDCIds;
	}

	public void setSelectedDCIds(List<DCListTO> selectedDCIds) {
		this.selectedDCIds = selectedDCIds;
	}

	public List<DCListTO> getDcList() {
		return dcList;
	}

	public void setDcList(List<DCListTO> dcList) {
		this.dcList = dcList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}

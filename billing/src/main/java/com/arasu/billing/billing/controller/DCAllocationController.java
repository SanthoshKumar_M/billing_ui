package com.arasu.billing.billing.controller;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleSelectEvent;
import org.primefaces.event.UnselectEvent;

import com.arasu.billing.base.controller.BaseController;
import com.arasu.billing.billing.bean.DCAllocationBean;
import com.arasu.billing.billing.service.DCService;
import com.arasu.billing.billing.to.DCListTO;
import com.arasu.billing.common.BillingConstant;
import com.arasu.billing.common.Messages;
import com.arasu.billing.common.paymentmode.PaymentMode;

@Named
@RequestScoped
public class DCAllocationController extends BaseController {

	private DCAllocationBean dcAllocationBean;
	private DCService dcService;
	boolean officeItemBranchValidationresult = true;

	@Inject
	public DCAllocationController(final DCAllocationBean dcAllocationBean, final DCService dcService,
			final Messages messages) {
		super(messages);
		this.dcAllocationBean = dcAllocationBean;
		this.dcService = dcService;
	}

	public void getDCList() {
		dcAllocationBean.setDcList(dcService.getDCList(dcAllocationBean.getBranchIds(),
				new Date(dcAllocationBean.getSelectedDCDate().getTime()), dcAllocationBean.getCityIds()));
	}

	public void calculateAmount(DCListTO dcListTO) {
		BigDecimal amount = dcListTO.getAmount() == null ? new BigDecimal(0) : dcListTO.getAmount();
		BigDecimal otherChargesAmount = dcListTO.getOtherChargesAmount() == null ? new BigDecimal(0)
				: dcListTO.getOtherChargesAmount();
		dcListTO.setNetAmount(amount.add(otherChargesAmount));
	}

	public void saveDCAllocation() {
		if (validate()) {
			setDCAllocatedDetails();
			int result = dcService.updateDCAllocations(dcAllocationBean.getSelectedDCIds());
			if (result > 0) {
				clear();
				getMessages().displayInfoMessage(null, "Success", result + " DC Updated Successfully.");
			} else {
				getMessages().displayErrorMessage(null, "Error", "DC Not Updated.");
			}
		} else {
			getMessages().displayErrorMessage(null, "Error",
					"DC Not Updated. Please Select Driver and Please Select Vehicle and Select DC in List");
		}
	}

	private void setDCAllocatedDetails() {
		dcAllocationBean.getSelectedDCIds().forEach(dcline -> {
			dcline.setDriverId(dcAllocationBean.getSelectedDriverId());
			dcline.setVehicleId(dcAllocationBean.getSelectedVehicleId());
			dcline.setDeliveredBy(dcAllocationBean.getSelectedDriverId());
			if (dcline.getModeOfPayment() == Integer.parseInt(PaymentMode.getModeOfPaymentList().stream()
					.filter(pay -> pay.getLabel().equals(BillingConstant.TOPAY_PAYMENT_MODE)).findAny().get().getValue()
					.toString())) {
				dcline.setAmountReceivedBy(dcAllocationBean.getSelectedDriverId());
			}
		});
	}

	public void isOfficeItem(DCListTO dcline) {
		String branchName = dcline.getBranchName();
		if (dcline.isOfficeDelivery()) {
			if (branchName.equalsIgnoreCase("Kattoor") || branchName.equalsIgnoreCase("Edayar Street")) {
				dcline.setOfficeId(2);
			} else {
				dcline.setOfficeId(1);
			}
		} else {
			dcline.setOfficeId(0);
		}
	}

	private boolean validate() {
		return isDCSelected() && isDriverSelected() && isVehicleSelected() && validateDeliveryBranch();
	}

	private boolean isVehicleSelected() {
		return dcAllocationBean.getSelectedVehicleId() > 0;
	}

	private boolean isDriverSelected() {
		return dcAllocationBean.getSelectedDriverId() > 0;
	}

	private boolean isDCSelected() {
		return !dcAllocationBean.getSelectedDCIds().isEmpty();
	}

	private Boolean validateDeliveryBranch() {
		for (DCListTO dcline : dcAllocationBean.getSelectedDCIds()) {
			if (dcline.isOfficeDelivery()) {
				if (dcline.getOfficeId() == 0) {
					getMessages().displayErrorMessage(null, "Error",
							"DC Not Updated. \n"+dcline.getdCNo()+" Select the Office Delivery Branch.");
					return Boolean.FALSE;
				} else if (dcline.getBranchName().equalsIgnoreCase("Kattoor")
						|| dcline.getBranchName().equalsIgnoreCase("Edayar Street")) {
					if (dcline.getOfficeId() != 2) {
						getMessages().displayErrorMessage(null, "Error",
								"DC Not Updated. \n"+dcline.getdCNo()+" Check the Office Delivery Branch.");
						return Boolean.FALSE;
					}
				} else {
					if (dcline.getOfficeId() == 2) {
						getMessages().displayErrorMessage(null, "Error",
								"DC Not Updated. \n"+dcline.getdCNo()+" Check the Office Delivery Branch.");
						return Boolean.FALSE;
					}
				}
			}
		}
		return Boolean.TRUE;
	}

	public boolean checkPaymentMode(int modeOfPayment) {
		return modeOfPayment == Integer.parseInt(PaymentMode.getModeOfPaymentList().stream()
				.filter(pay -> pay.getLabel().equals(BillingConstant.TOPAY_PAYMENT_MODE)).findAny().get().getValue()
				.toString());
	}

	public void setSelectedID(int selectedID) {
		HttpSession httpSession = dcAllocationBean.getCurrentInstance().getHttpSession();
		httpSession.setAttribute("selectedID", selectedID);
	}

	private void clear() {
		dcAllocationBean.setDcList(null);
		dcAllocationBean.getSelectedDCIds().clear();
		dcAllocationBean.getCityIds().clear();
		dcAllocationBean.getBranchIds().clear();
		dcAllocationBean.setSelectedDriverId(0);
		dcAllocationBean.setSelectedVehicleId(0);
	}

	
	public void onDcScan() {
	    boolean isIdPresent = dcAllocationBean.getSelectedDCIds().stream()
	            .anyMatch(obj -> obj.getId() == dcAllocationBean.getScanDCId());
	    if (!isIdPresent) {
	        Optional<DCListTO> sacnDC = dcAllocationBean.getDcList().stream()
	                .filter(obj -> obj.getId() == dcAllocationBean.getScanDCId()).findAny();
	        if (sacnDC.isPresent()) {
	        	dcAllocationBean.getSelectedDCIds().add(sacnDC.get());
	        	dcAllocationBean.setScanDCId(0);
	            sortDcListBySelectedIds();
	        } else {
	            getMessages().displayErrorMessage(null, "Error", "Invalid. Please scan valid DC.");
	        }
	    }
	    dcAllocationBean.setScanDCId(0);
	    select("input_dcno_id");
	}

	private void sortDcListBySelectedIds() {
	    List<DCListTO> selectedDCIds = dcAllocationBean.getSelectedDCIds();
	    dcAllocationBean.getDcList().sort(Comparator.comparingInt(dc -> {
	        int index = selectedDCIds.indexOf(dc);
	        return index != -1 ? index : Integer.MAX_VALUE;
	    }));
	}
	
	
	public void onRowSelect(SelectEvent<DCListTO> event) {
		sortDcListBySelectedIds();
	}



	public void onRowUnSelect(UnselectEvent<DCListTO> event) {
		sortDcListBySelectedIds();
	}
	
	
	public void onAllRowsSelected(ToggleSelectEvent event) {
		if (event.isSelected()) {
			sortDcListBySelectedIds();
		} else {
			dcAllocationBean.getSelectedDCIds().clear();
		}
	}

	
}

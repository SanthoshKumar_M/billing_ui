package com.arasu.billing.billing.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleSelectEvent;
import org.primefaces.event.UnselectEvent;

import com.arasu.billing.base.controller.BaseController;
import com.arasu.billing.billing.bean.DCCollectionBean;
import com.arasu.billing.billing.service.DCService;
import com.arasu.billing.billing.to.DCListTO;
import com.arasu.billing.common.BillingConstant;
import com.arasu.billing.common.Messages;
import com.arasu.billing.common.paymentmode.PaymentMode;

@Named
@RequestScoped
public class DCCollectionController extends BaseController {

	private DCCollectionBean dCCollectionBean;
	private DCService dcService;

	@Inject
	public DCCollectionController(final DCCollectionBean dCCollectionBean, final DCService dcService,
			final Messages messages) {
		super(messages);
		this.dCCollectionBean = dCCollectionBean;
		this.dcService = dcService;
	}

	public void getDCList() {
		dCCollectionBean.getSelectedDCIds().clear();
		if (dCCollectionBean.getSelectedDriverId().equalsIgnoreCase("Office")) {
			dCCollectionBean.setDcList(
					dcService.getOfficeDCList(dCCollectionBean.getCurrentInstance().getLoginBranchDetails().getId()));

		} else {
			dCCollectionBean
					.setDcList(dcService.getDriverDCList(Integer.parseInt(dCCollectionBean.getSelectedDriverId())));
		}
	}

	public void saveDCCollectionDetails() {
		if (validate()) {
			dCCollectionBean.getSelectedDCIds().stream()
					.peek(dCData -> dCData.setdCAmountCheckBy(dCCollectionBean.getCurrentInstance().getLoginUserID()))
					.collect(Collectors.toList());
			int result = dcService.updateDCWithCollectionValue(dCCollectionBean.getSelectedDCIds());
			if (result > 0) {
				clear();
				getMessages().displayInfoMessage(null, "Success", result + " DC Updated Successfully.");
			} else {
				getMessages().displayErrorMessage(null, "Error", "DC Not Updated.");
			}
		} else {
			getMessages().displayErrorMessage(null, "Error",
					"DC Not Updated. Please Select Driver and Select DC in List");
		}
	}

	private boolean validate() {
		return isDCSelected() && isDriverSelected();
	}

	private boolean isDriverSelected() {
		if (dCCollectionBean.getSelectedDriverId().equalsIgnoreCase("Office")) {
			return Boolean.TRUE;
		} else {
			return Integer.parseInt(dCCollectionBean.getSelectedDriverId()) > 0;
		}
	}

	private boolean isDCSelected() {
		return !dCCollectionBean.getSelectedDCIds().isEmpty();
	}

	public String getDeliveryStatus(boolean deliveryStatus) {
		if (deliveryStatus) {
			return BillingConstant.DELIVERY_STATUS_DONE_STR;
		} else {
			return BillingConstant.DELIVERY_STATUS_PENDING_STR;
		}
	}

	public String getPaymentStatus(boolean received) {
		if (received) {
			return BillingConstant.PAYMENT_STATUS_DONE_STR;
		}
		return BillingConstant.PAYMENT_STATUS_PENDING_STR;
	}

	public void onRowSelect(SelectEvent<DCListTO> event) {
		DCListTO dcListTO = event.getObject();
		if (getPaymentMode(dcListTO)) {
			dcListTO.setReceivedAmount(dcListTO.getNetAmount());
//			dCCollectionBean.getSelectedDCIds().forEach(dcline -> {
//				if (dcline.getId() == dcListTO.getId()) {
//					dcline.setReceivedAmount(dcListTO.getReceivedAmount());
//				}
//			});
		}
		sortDcListBySelectedIds();
	}

	private boolean getPaymentMode(DCListTO dcListTO) {
		return dcListTO.getModeOfPayment() == Integer.parseInt(PaymentMode.getModeOfPaymentList().stream()
				.filter(pay -> pay.getLabel().equals(BillingConstant.TOPAY_PAYMENT_MODE)).findAny().get().getValue()
				.toString());
	}

	public void onRowUnSelect(UnselectEvent<DCListTO> event) {
		DCListTO dcListTO = event.getObject();
		if (getPaymentMode(dcListTO)) {
			dcListTO.setReceivedAmount(BigDecimal.valueOf(0.00));
//			dCCollectionBean.getSelectedDCIds().forEach(dcline -> {
//				if (dcline.getId() == dcListTO.getId()) {
//					dcline.setReceivedAmount(new BigDecimal(0.00));
//				}
//			});
		}
		sortDcListBySelectedIds();
	}

	public void onAllRowsSelected(ToggleSelectEvent event) {
		dCCollectionBean.getDcList().forEach(dcline -> {
			if (getPaymentMode(dcline)) {
				if (event.isSelected()) {
					dcline.setReceivedAmount(dcline.getNetAmount());
				} else {
					dcline.setReceivedAmount(BigDecimal.valueOf(0.00));
				}
			}
		});

//		dCCollectionBean.getSelectedDCIds().forEach(dcline -> {
//			if (getPaymentMode(dcline)) {
//				if (event.isSelected()) {
//					dcline.setReceivedAmount(dcline.getNetAmount());
//				} else {
//					dcline.setReceivedAmount(new BigDecimal(0.00));
//				}
//			}
//		});
	}

	public BigDecimal calculateTotalCollectionAmount() {
		dCCollectionBean.setTotalAmount(BigDecimal.valueOf(0.00));
		dCCollectionBean.getSelectedDCIds().forEach(dcline -> {
			if (getPaymentMode(dcline)) {
				dCCollectionBean.setTotalAmount(dCCollectionBean.getTotalAmount().add(dcline.getReceivedAmount()));
			}
		});
		return dCCollectionBean.getTotalAmount().setScale(2);
	}

	private void clear() {
		dCCollectionBean.setDcList(new ArrayList<>());
		dCCollectionBean.getSelectedDCIds().clear();
		dCCollectionBean.setSelectedDriverId(BillingConstant.ZERO.toString());
	}

	public void onDcScan() {
	    boolean isIdPresent = dCCollectionBean.getSelectedDCIds().stream()
	            .anyMatch(obj -> obj.getId() == dCCollectionBean.getScanDCId());
	    if (!isIdPresent) {
	        Optional<DCListTO> sacnDC = dCCollectionBean.getDcList().stream()
	                .filter(obj -> obj.getId() == dCCollectionBean.getScanDCId()).findAny();
	        if (sacnDC.isPresent()) {
	            dCCollectionBean.getSelectedDCIds().add(sacnDC.get());
	            dCCollectionBean.setScanDCId(0);
	            if (getPaymentMode(sacnDC.get())) {
	                sacnDC.get().setReceivedAmount(sacnDC.get().getNetAmount());
	            }
	            sortDcListBySelectedIds();
	        } else {
	            getMessages().displayErrorMessage(null, "Error", "Invalid. Please scan valid DC.");
	        }
	    }
	    dCCollectionBean.setScanDCId(0);
	    select("input_dcno_id");
	}

	private void sortDcListBySelectedIds() {
	    List<DCListTO> selectedDCIds = dCCollectionBean.getSelectedDCIds();
	    dCCollectionBean.getDcList().sort(Comparator.comparingInt(dc -> {
	        int index = selectedDCIds.indexOf(dc);
	        return index != -1 ? index : Integer.MAX_VALUE;
	    }));
	}


}

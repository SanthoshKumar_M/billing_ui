package com.arasu.billing.billing.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.admin.service.DCRunningNumberService;
import com.arasu.billing.base.controller.BaseController;
import com.arasu.billing.billing.bean.DCBean;
import com.arasu.billing.billing.service.DCService;
import com.arasu.billing.billing.to.DCLineTO;
import com.arasu.billing.common.BillingConstant;
import com.arasu.billing.common.BillingStriingUtils;
import com.arasu.billing.common.Messages;
import com.arasu.billing.common.paymentmode.PaymentMode;
import com.arasu.billing.common.sugessionlistbean.CustomerSuggessionListBean;
import com.arasu.billing.data.DCData;
import com.arasu.billing.master.controller.BranchController;
import com.arasu.billing.master.service.CityMasterService;
import com.arasu.billing.master.service.CustomerMasterService;
import com.arasu.billing.master.service.ItemMasterService;
import com.arasu.billing.master.to.CityMasterTO;
import com.arasu.billing.master.to.CustomerMasterTO;
import com.arasu.billing.master.to.ItemMasterTO;
import com.arasu.billing.print.ReportGenerator;

@Named
@RequestScoped
public class DCController extends BaseController {

	private DCBean dCBean;
	private DCService dcService;
	private CustomerMasterService customerMasterService;
	private CityMasterService cityMasterService;
	private ItemMasterService itemMasterService;
	private DCRunningNumberService dCRunningNumberService;
	private ReportGenerator reportGenerator;
	private BranchController branchController;

	@Inject
	public DCController(final DCBean dCBean, final DCService dcService,
			final CustomerMasterService customerMasterService, final CityMasterService cityMasterService,
			final ItemMasterService itemMasterService, final DCRunningNumberService dCRunningNumberService,
			final ReportGenerator reportGenerator, final Messages messages, final BranchController branchController) {
		super(messages);
		this.dCBean = dCBean;
		this.dcService = dcService;
		this.customerMasterService = customerMasterService;
		this.cityMasterService = cityMasterService;
		this.itemMasterService = itemMasterService;
		this.dCRunningNumberService = dCRunningNumberService;
		this.reportGenerator = reportGenerator;
		this.branchController = branchController;

	}

	public int getCurrentDCRunningNumber() {
		if (dCBean.getDcData().getDc().getdCNo() > 0) {
			return dCBean.getDcData().getDc().getdCNo();
		}
		return dCRunningNumberService
				.getCurrentDCRunningNumber(dCBean.getCurrentInstance().getLoginBranchDetails().getId());
	}

	public List<CustomerMasterTO> customerNameSuggestionList(String suggestionValue) {
		suggestionValue = BillingStriingUtils.capitalize(suggestionValue);
		List<CustomerMasterTO> customerMasterTOs;
		customerMasterTOs = customerMasterService.getcustomerList(suggestionValue);
		if (customerMasterTOs.isEmpty() && !suggestionValue.trim().isEmpty()) {
			customerMasterTOs.addAll(customerMasterService.getTranslationCustomerName(suggestionValue));
			CustomerSuggessionListBean.getCustomerSuggestionList().addAll(customerMasterTOs);
		}
		return customerMasterTOs;
	}

	public List<CityMasterTO> citysSuggestionList(String suggestionValue) {
		return cityMasterService.getCitysSuggestionList(suggestionValue);
	}

	public List<ItemMasterTO> itemNameSuggestion(String suggestionValue) {
		return itemMasterService.itemNameSuggestionList(suggestionValue);
	}

	public void getFromCustomerDetailsById() {
		if (dCBean.getDcData().getDc().getFromCustomerMaster().getId() > 0) {
			dCBean.getDcData().getDc().setFromCustomerMaster(
					getCustomerDetailsById(dCBean.getDcData().getDc().getFromCustomerMaster().getId()));
			dCBean.setFromCustomerCityMaster(dCBean.getDcData().getDc().getFromCustomerMaster().getCityMaster());
		}
	}

	public void getToCustomerDetailsById() {
		if (dCBean.getDcData().getDc().getToCustomerMaster().getId() > 0) {
			dCBean.getDcData().getDc().setToCustomerMaster(
					getCustomerDetailsById(dCBean.getDcData().getDc().getToCustomerMaster().getId()));
			dCBean.getDcData().getDc().setToCustomerMobileNumber(dCBean.getDcData().getDc().getToCustomerMaster().getMobileNumber());
		}
	}

	private CustomerMasterTO getCustomerDetailsById(int id) {
		return customerMasterService.getCustomerDetailsById(id);
	}

	public void setFromCityDetails() {
		if (dCBean.getDcData().getDc().getFromCustomerMaster().getId() == 0) {
			dCBean.getDcData().getDc().getFromCustomerMaster().setCityMaster(dCBean.getFromCustomerCityMaster());
		}
	}

	public String getFromCustomerAddress() {
		return null != dCBean.getDcData().getDc().getFromCustomerMaster()
				? getCustomerAddress(dCBean.getDcData().getDc().getFromCustomerMaster())
				: "";
	}

	public String getToCustomerAddress() {
		return null != dCBean.getDcData().getDc().getToCustomerMaster()
				? getCustomerAddress(dCBean.getDcData().getDc().getToCustomerMaster())
				: "";
	}

	private String getCustomerAddress(CustomerMasterTO customerMasterTO) {
		return getAddress(customerMasterTO) + "\n" + getCity(customerMasterTO);
	}

	private String getCity(CustomerMasterTO customerMasterTO) {
		return null != customerMasterTO.getCustomerAddress() ? customerMasterTO.getCustomerAddress()
				: BillingConstant.EMPTY_STRING;
	}

	private String getAddress(CustomerMasterTO customerMasterTO) {
		return null != customerMasterTO.getCityMaster() ? customerMasterTO.getCityMaster().getCityName()
				: BillingConstant.EMPTY_STRING;
	}

	public int totalNumberOfQty() {
		int sumOfTotalQty = (int) dCBean.getDcData().getDcLine().stream()
				.filter(i -> (i.getItemMaster() != null && i.getItemMaster().getId() > 0))
				.mapToDouble(mapper -> mapper.getQty()).sum();
		dCBean.getDcData().getDc().setTotalNoOfQty(sumOfTotalQty);
		return sumOfTotalQty;
	}

	public void addNewRow() {
		int listSize = dCBean.getDcData().getDcLine().size();
		if (null != dCBean.getDcData().getDcLine().get(listSize - 1).getItemMaster()) {
			if (dCBean.getDcData().getDcLine().get(listSize - 1).getId() == 0) {
				dCBean.getDcData().getDcLine().get(listSize - 1).setQty(1);
			}
			if (listSize > 0 && listSize < 7) {
				dCBean.addNewRowItemList();
			}
		}
	}

	public void otherCharges() {
		if (dCBean.getDcData().getDc().getOtherChargesDetails().trim().isEmpty()) {
			dCBean.getDcData().getDc().setOtherChargesAmount(new BigDecimal(0));
		}
		calculateTotalAmount();
	}

	public void calculateTotalAmount() {
		BigDecimal amount = dCBean.getDcData().getDc().getAmount();
		amount = amount == null ? BillingConstant.getZeroAmount() : amount;
		BigDecimal otherAmount = dCBean.getDcData().getDc().getOtherChargesAmount();
		BigDecimal otherCharegesAmount = otherAmount == null ? BillingConstant.getZeroAmount() : otherAmount;
		dCBean.getDcData().getDc().setNetAmount(amount.add(otherCharegesAmount));
	}

	public void saveOrUpdateDCDetails() {
		int id = dCBean.getDcData().getDc().getId();
		if (0 == id) {
			saveDCDetails(BillingConstant.TYPE_I);
		} else {
			saveDCDetails(BillingConstant.TYPE_U);
		}

	}

	public void cancelDC() {
		setCancelToDCAndDCLine();
		saveOrUpdateDCDetails();
	}

	private void setCancelToDCAndDCLine() {
		dCBean.getDcData().getDc().setRowStatus(BillingConstant.CANCELED);
		dCBean.getDcData().getDcLine().forEach(dcline -> {
			if (dcline.getId() > 0) {
				dcline.setRowStatus(BillingConstant.CANCELED);
			} else if (null != dcline.getItemMaster() && dcline.getId() == 0) {
				dCBean.getDcData().getDcLine().remove(dcline);
			}
		});
	}

	public void saveDCDetails(char type) {

		if (dcValidator()) {
			DCData dcData = dcService.saveOrUpdate(dCBean.getDcData(), type);
			if (null != dcData && dcData.getDc().getId() > 0) {
				getPaymentMode(dcData);
				dcData.getParameters().put("forArasu",
						dCBean.getCurrentInstance().getLoginEmployeeDetails().getEmployeeName());
				dcData.getParameters().put("itemList", dcData.getReportTOs());
				reportGenerator.print("dc.jrxml",Boolean.FALSE ,dcData.getParameters(), dcData.getReportTOs());
				clear();
				customerMasterService.getNewCustomerList();
				if (type == BillingConstant.TYPE_I) {
					getMessages().displayInfoMessage(null, "Success", "DC Create Successfully.");
				} else {
					getMessages().displayInfoMessage(null, "Success", "DC Update Successfully.");
				}
			} else {
				getMessages().displayErrorMessage(null, "Error", "DC Not Created.");
			}
		} 
//		else {
//			getMessages().displayErrorMessage(null, "Error", "Not a Valid DC.");
//		}
		primefacesUpdate("main_outputpanel_id");
	}

	private void getPaymentMode(DCData dcData) {
		String paymetMode = PaymentMode.getModeOfPaymentList().stream()
				.filter(pay -> pay.getValue().equals(dcData.getParameters().get("paymentMode"))).findAny().get()
				.getLabel();
		dcData.getParameters().put("paymentMode", paymetMode);
	}

	private boolean dcValidator() {
	    return validateItemList() && validateQty() && validateAmount();
	}

	private boolean validateQty() {
	    List<DCLineTO> dclineList = dCBean.getDcData().getDcLine().stream()
	            .filter(dcline -> dcline.getItemMaster() != null && dcline.getQty() == 0)
	            .collect(Collectors.toList());
	    if (!dclineList.isEmpty()) {
	        getMessages().displayErrorMessage(null, "Error", "One or more items have a quantity of zero.");
	        return false;
	    }
	    return true;
	}

	private boolean validateItemList() {
	    List<DCLineTO> dclineList = dCBean.getDcData().getDcLine().stream()
	            .filter(dcline -> dcline.getItemMaster() != null)
	            .collect(Collectors.toList());
	    if (dclineList.isEmpty()) {
	        getMessages().displayErrorMessage(null, "Error", "Item list cannot be empty.");
	        return false;
	    }
	    return true;
	}

	private boolean validateAmount() {
	    boolean validateResult = true;
	    dCBean.getDcData().getDc().setReceivedAmount(new BigDecimal(0));
	    if (checkPaymentMode() && dCBean.getDcData().getDc().getAmount().compareTo(new BigDecimal(0)) == 0) {
	        validateResult = false;
	        getMessages().displayErrorMessage(null, "Error", "Total Amount is Zero!..");
	    } else if (isPaidPaymentMode()) {
	        dCBean.getDcData().getDc().setAmountReceivedBy(dCBean.getCurrentInstance().getEmployeeMaster());
	        dCBean.getDcData().getDc().setAmountReceivedDate(new Date());
	        dCBean.getDcData().getDc().setReceivedAmount(dCBean.getDcData().getDc().getNetAmount());
	    }
	    return validateResult;
	}

	private boolean checkPaymentMode() {
		return dCBean.getDcData().getDc().getModeOfPay() != isAccPaymentMode();
	}

	private int isAccPaymentMode() {
		return Integer.parseInt(PaymentMode.getModeOfPaymentList().stream()
				.filter(pay -> pay.getLabel().equals(BillingConstant.ACC_PAYMENT_MODE)).findAny().get().getValue()
				.toString());
	}

	private boolean isPaidPaymentMode() {
		return dCBean.getDcData().getDc()
				.getModeOfPay() == Integer.parseInt(PaymentMode.getModeOfPaymentList().stream()
						.filter(pay -> pay.getLabel().equals(BillingConstant.PAID_PAYMENT_MODE)).findAny().get()
						.getValue().toString());
	}

	public List<SelectItem> getModeOfPay() {
		return PaymentMode.getModeOfPaymentList();
	}

	public void getDCList() {
		dCBean.getdCList().clear();
		if (dCBean.getSearchDCNo() == 0) {
			dCBean.getdCList().addAll(dcService.getDCList(dCBean.getCurrentInstance().getLoginBranchDetails().getId()));
		} else {
			dCBean.getdCList().addAll(dcService.getDCList(dCBean.getCurrentInstance().getLoginBranchDetails().getId(),
					dCBean.getSearchDCNo()));
		}
	}

	public void getDCDetailsById(int dCId) {
		dCBean.setDcData(dcService.getDCDatailsById(dCId));
		dCBean.setFromCustomerCityMaster(dCBean.getDcData().getDc().getFromCustomerMaster().getCityMaster());
		dCBean.setSearchDCNo(0);
		updateUser();
		calculateTotalAmount();
		addNewRow();
	}

	private void updateUser() {
		dCBean.getDcData().getDc().setUpdateUser(dCBean.getCurrentInstance().getEmployeeMaster());
		dCBean.getDcData().getDcLine()
				.forEach(dcline -> dcline.setUpdateUser(dCBean.getCurrentInstance().getEmployeeMaster()));
	}

	public void removeDCLineItem(int index) {
		getRemoveDCLineIds(index);
		dCBean.getDcData().getDcLine().remove(index);
		if (dCBean.getDcData().getDcLine().isEmpty()) {
			dCBean.addNewRowItemList();
		} else {
			addNewRow();
		}

	}

	private void getRemoveDCLineIds(int index) {
		int removeId = dCBean.getDcData().getDcLine().get(index).getId();
		if (removeId > 0) {
			dCBean.getDcData().getRemoveItemIds().add(removeId);
		}
	}

	public String getDCDeliveryStatus(int deliveryStatus) {
		return BillingConstant.getDeliveryStatusMap().get(deliveryStatus);
	}

	public String getDCStatus(int dcStatus) {
		return BillingConstant.getDcStatusMap().get(dcStatus);
	}

	public String getPaymentStatus(BigDecimal receivedAmount) {
		if (null == receivedAmount || receivedAmount.compareTo(new BigDecimal(0)) == 0) {
			return BillingConstant.PAYMENT_STATUS_PENDING_STR;
		}
		return BillingConstant.PAYMENT_STATUS_DONE_STR;

	}

	public boolean checkSubmit() {
		boolean isSubmitAvaiable = Boolean.TRUE;
		if (getDCDeliveryStatus(dCBean.getDcData().getDc().getDeliveryStatus())
				.equalsIgnoreCase(BillingConstant.DELIVERY_STATUS_PENDING_STR)
				&& getDCStatus(dCBean.getDcData().getDc().getRowStatus())
						.equalsIgnoreCase(BillingConstant.DC_STATUS_ACTIVE_STR)) {
			isSubmitAvaiable = Boolean.FALSE;
		}
		return isSubmitAvaiable;
	}

	public void clear() {
		dCBean.setDcData(new DCData());
		dCBean.setFromCustomerCityMaster(null);
		dCBean.init();
	}

	public void redirectDC() {
		if (dCBean.getCurrentInstance().getHttpSession().getAttribute("selectedID") != null) {
			getDCDetailsById((int) dCBean.getCurrentInstance().getHttpSession().getAttribute("selectedID"));
			dCBean.getCurrentInstance().getHttpSession().removeAttribute("selectedID");
		}

	}

	public boolean isOfficeItem() {
		int branchId = dCBean.getCurrentInstance().getLoginBranchDetails().getId();
		int dcId = dCBean.getDcData().getDc().getId();
		if (dCBean.getDcData().getDc().isOfficeDelivery()) {
			if (branchId == 1 || branchId == 3) {
				if (dcId == 0) {
					dCBean.getDcData().getDc().setOfficeId(2);
				}
				return Boolean.FALSE;
			} else {
				if (dcId == 0) {
					dCBean.getDcData().getDc().setOfficeId(1);
				}
				return Boolean.TRUE;
			}
		} else {
			return Boolean.FALSE;
		}
	}

	public List<SelectItem> getOfficeList() {
		List<Integer> branchIds = new ArrayList<>();
		branchIds.add(2);
		return branchController.getAllBranchSelectItems(branchIds);
	}
}

package com.arasu.billing.billing.controller;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.base.controller.BaseController;
import com.arasu.billing.billing.bean.DCTransferBean;
import com.arasu.billing.billing.service.DCService;
import com.arasu.billing.common.BillingConstant;
import com.arasu.billing.common.Messages;
import com.arasu.billing.common.paymentmode.PaymentMode;

@Named
@RequestScoped
public class DCTransferController extends BaseController {

	private DCTransferBean dCTransferBean;
	private DCService dcService;

	@Inject
	public DCTransferController(final DCTransferBean dcAllocationBean, final DCService dcService, final Messages messages) {
		super(messages);
		this.dCTransferBean = dcAllocationBean;
		this.dcService = dcService;
	}

	public void getDCList() {
		dCTransferBean.setDcList(dcService.getDriverDCList(dCTransferBean.getSelectedDriverId()));
	}

	public void saveDCTransfer() {
		if (validate()) {
			setDCTransferValues();
			int result = dcService.updateDCWithTransferValue(dCTransferBean.getSelectedDCIds());
			if (result > 0) {
				clear();
				getMessages().displayInfoMessage(null, "Success", result + " DC Updated Successfully.");
			} else {
				getMessages().displayErrorMessage(null, "Error", "DC Not Updated.");
			}
		} else {
			getMessages().displayErrorMessage(null, "Error", "DC Not Updated. Please Select Driver, Transfer Driver and Select DC in List");
		}
	}

	private void setDCTransferValues() {
		dCTransferBean.getSelectedDCIds().forEach(dcline -> {
			if (dcline.getDeliveryStatus() == BillingConstant.DELIVERY_STATUS_PENDING) {
				dcline.setDeliveredBy(dCTransferBean.getTransferDriverId());
				if (dcline.getModeOfPayment() == Integer.parseInt(PaymentMode.getModeOfPaymentList().stream()
						.filter(pay -> pay.getLabel().equals(BillingConstant.TOPAY_PAYMENT_MODE)).findAny().get().getValue().toString())) {
					dcline.setAmountReceivedBy(dCTransferBean.getTransferDriverId());
				}
			} else if (dcline.getModeOfPayment() == Integer.parseInt(
					PaymentMode.getModeOfPaymentList().stream().filter(pay -> pay.getLabel().equals(BillingConstant.TOPAY_PAYMENT_MODE)).findAny().get().getValue().toString())) {
				dcline.setAmountReceivedBy(dCTransferBean.getTransferDriverId());
			}
		});
	}

	private boolean validate() {
		return isDCSelected() && isDriverSelected() && isTransferDriverSelected();
	}

	private boolean isTransferDriverSelected() {
		return dCTransferBean.getTransferDriverId() > 0;
	}

	private boolean isDriverSelected() {
		return dCTransferBean.getSelectedDriverId() > 0;
	}

	private boolean isDCSelected() {
		return !dCTransferBean.getSelectedDCIds().isEmpty();
	}

	public String getDeliveryStatus(boolean deliveryStatus) {
		if (deliveryStatus) {
			return BillingConstant.DELIVERY_STATUS_DONE_STR;
		} else {
			return BillingConstant.DELIVERY_STATUS_PENDING_STR;
		}
	}

	public String getPaymentStatus(boolean received) {
		if (received) {
			return BillingConstant.PAYMENT_STATUS_DONE_STR;
		}
		return BillingConstant.PAYMENT_STATUS_PENDING_STR;
	}

	public List<SelectItem> getPaymentStatus() {
		return BillingConstant.getPaymentStatusList();
	}

	public List<SelectItem> getDeliveryStatus() {
		return BillingConstant.getDeliveryStatusList();
	}

	private void clear() {
		dCTransferBean.setDcList(new ArrayList<>());
		dCTransferBean.getSelectedDCIds().clear();
		dCTransferBean.setSelectedDriverId(0);
		dCTransferBean.setTransferDriverId(0);
	}

}

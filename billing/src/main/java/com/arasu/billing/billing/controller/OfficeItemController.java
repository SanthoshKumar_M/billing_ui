package com.arasu.billing.billing.controller;

import java.math.BigDecimal;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import com.arasu.billing.base.controller.BaseController;
import com.arasu.billing.billing.bean.OfficeItemBean;
import com.arasu.billing.billing.service.DCService;
import com.arasu.billing.billing.to.DCListTO;
import com.arasu.billing.common.BillingConstant;
import com.arasu.billing.common.Messages;
import com.arasu.billing.common.paymentmode.PaymentMode;

@Named
@RequestScoped
public class OfficeItemController extends BaseController {

	private OfficeItemBean officeItemBean;
	private DCService dcService;

	@Inject
	public OfficeItemController(final OfficeItemBean officeItemBean, final DCService dcService, final Messages messages) {
		super(messages);
		this.officeItemBean = officeItemBean;
		this.dcService = dcService;
	}

	public void getDCList() {
		officeItemBean.setDcList(dcService.getDCList(officeItemBean.getCurrentInstance().getLoginBranchDetails()));
	}

	public void calculateAmount(DCListTO dcListTO) {
		BigDecimal amount = dcListTO.getAmount() == null ? new BigDecimal(0) : dcListTO.getAmount();
		BigDecimal otherChargesAmount = dcListTO.getOtherChargesAmount() == null ? new BigDecimal(0) : dcListTO.getOtherChargesAmount();
		dcListTO.setNetAmount(amount.add(otherChargesAmount));
	}

	public void saveRecivedOfficeItem() {
		if (validate()) {
			int result = dcService.receivedOfficeItemDetails(officeItemBean.getSelectedDCIds());
			if (result > 0) {
				clear();
				getMessages().displayInfoMessage(null, "Success", result + " Office Items Received Successfully.");
			} else {
				getMessages().displayErrorMessage(null, "Error", " Office Items Not Received.");
			}
		} else {
			getMessages().displayErrorMessage(null, "Error", "Please Select DC Ids in List");
		}
	}


	private boolean validate() {
		return isDCSelected();
	}

	private boolean isDCSelected() {
		return !officeItemBean.getSelectedDCIds().isEmpty();
	}

	public boolean checkPaymentMode(int modeOfPayment) {
		return modeOfPayment == Integer.parseInt(
				PaymentMode.getModeOfPaymentList().stream().filter(pay -> pay.getLabel().equals(BillingConstant.TOPAY_PAYMENT_MODE)).findAny().get().getValue().toString());
	}
	
	private void clear() {
		officeItemBean.setDcList(null);
		officeItemBean.getSelectedDCIds().clear();
	}
	
	public void setSelectedID(int selectedID) {
		HttpSession httpSession = officeItemBean.getCurrentInstance().getHttpSession();
		httpSession.setAttribute("selectedID", selectedID);
	}

}

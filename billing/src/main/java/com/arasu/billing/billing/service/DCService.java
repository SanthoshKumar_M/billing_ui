package com.arasu.billing.billing.service;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.base.service.BaseService;
import com.arasu.billing.billing.to.DCListTO;
import com.arasu.billing.billing.to.DCTO;
import com.arasu.billing.common.BillingConstant;
import com.arasu.billing.data.DCData;
import com.arasu.billing.master.to.BranchMasterTO;
import com.arasu.billing.service.HttpClientService;

@Named
@RequestScoped
public class DCService extends BaseService {

	public DCService() {

	}

	@Inject
	public DCService(final HttpClientService httpClientService) {
		super(httpClientService);
	}

	public DCData saveOrUpdate(DCData dcData, char type) {
		if (type == BillingConstant.TYPE_I) {
			return getHttpClientService().post("saveDCDetails", dcData, DCData.class);
		} else {
			return getHttpClientService().put("updateDCDetails", dcData, DCData.class);
		}
	}

	public List<DCTO> getDCList(int branchId) {
		return Arrays.asList(getHttpClientService().get("getDCList/" + branchId, DCTO[].class));
	}

	public DCData getDCDatailsById(int dCId) {
		return getHttpClientService().get("getDCDetailsById/" + dCId, DCData.class);
	}

	public List<DCTO> getDCList(int branchId, int searchDCNo) {
		return Arrays.asList(getHttpClientService().get("getDCList/" + branchId + "/" + searchDCNo, DCTO[].class));
	}

	public List<DCListTO> getDCList(List<Integer> branchIds, Date selectedDCDate, List<Integer> cityIds) {
		StringBuilder cityId = getCityMastersIDs(cityIds);
		StringBuilder branchId = getBranchMastersIDs(branchIds);
		return Arrays.asList(getHttpClientService().get("getDCList/" + branchId + "/" + selectedDCDate + "/" + cityId, DCListTO[].class));
	}

	public int updateDCAllocations(List<DCListTO> dcListTOs) {
		return getHttpClientService().put("updateDCAllocations", dcListTOs, Integer.class);
	}

	public List<DCListTO> getDriverDCList(int selectedDriverId) {
		return Arrays.asList(getHttpClientService().get("getDriverDCList/" + selectedDriverId, DCListTO[].class));
	}

	public int updateDCWithTransferValue(List<DCListTO> selectedDCs) {
		return getHttpClientService().put("updateDCTransfer", selectedDCs, Integer.class);
	}

	public int updateDCWithCollectionValue(List<DCListTO> selectedDCIds) {
		return getHttpClientService().put("updateDCCollection", selectedDCIds, Integer.class);
	}

	public List<DCListTO> getDCList(BranchMasterTO loginBranchDetails) {
		return Arrays.asList(getHttpClientService().get("getOfficeItemDCList/" + loginBranchDetails.getId(), DCListTO[].class));
	}
	
	public int receivedOfficeItemDetails(List<DCListTO> dcListTOs) {
		return getHttpClientService().put("receivedOfficeItemDetails", dcListTOs, Integer.class);
	}
	
	public List<DCListTO> getOfficeDCList(int branchId) {
		return Arrays.asList(getHttpClientService().get("getReceivedOfficeItemDCList/" + branchId, DCListTO[].class));
	}

	private StringBuilder getCityMastersIDs(List<Integer> cityIds) {
		StringBuilder cityId = new StringBuilder();
		for (int i = 0; i < cityIds.size(); i++) {
			if (cityId.length() == 0) {
				cityId.append(cityIds.get(i));
			} else {
				cityId.append("," + cityIds.get(i));
			}
		}
		return cityId;
	}

	private StringBuilder getBranchMastersIDs(List<Integer> branchIds) {
		StringBuilder branchId = new StringBuilder();
		for (int i = 0; i < branchIds.size(); i++) {
			if (branchId.length() == 0) {
				branchId.append(branchIds.get(i));
			} else {
				branchId.append("," + branchIds.get(i));
			}
		}
		return branchId;
	}

	public List<DCListTO> getMasterReport(Date fromDate, Date toDate, int customerMasterId, int cityMasterId, int driverId) {
		return Arrays.asList(getHttpClientService().get("getMasterReport/" + fromDate+ "/" +toDate+ "/" +customerMasterId+ "/" +cityMasterId+ "/" +driverId, DCListTO[].class));
	}

	public List<DCListTO> getMonthlyReport(Date fromDate, Date toDate, int customerId) {
		return Arrays.asList(getHttpClientService().get("getMonthlyReport/" + fromDate+ "/" +toDate+"/" +customerId , DCListTO[].class));

	}

}

package com.arasu.billing.billing.to;

import java.io.Serializable;

import com.arasu.billing.base.to.BaseTO;
import com.arasu.billing.common.BillingConstant;
import com.arasu.billing.master.to.EmployeeMasterTO;
import com.arasu.billing.master.to.ItemMasterTO;

public class DCLineTO extends BaseTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private DCTO dc;
	private ItemMasterTO itemMaster;
	private int qty;

	public DCLineTO(EmployeeMasterTO createUser) {
		qty = 0;
		setCreateUser(createUser);
		setRowStatus(BillingConstant.ACTIVE);
	}

	public DCLineTO() {

	}

	public ItemMasterTO getItemMaster() {
		return itemMaster;
	}

	public void setItemMaster(ItemMasterTO itemMaster) {
		this.itemMaster = itemMaster;
	}
	
	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public DCTO getDc() {
		return dc;
	}

	public void setDc(DCTO dc) {
		this.dc = dc;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}

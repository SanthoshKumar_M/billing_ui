package com.arasu.billing.billing.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.arasu.billing.base.to.BaseTO;
import com.arasu.billing.common.BillingConstant;
import com.arasu.billing.master.to.BranchMasterTO;
import com.arasu.billing.master.to.CustomerMasterTO;
import com.arasu.billing.master.to.EmployeeMasterTO;
import com.arasu.billing.master.to.VehicleMasterTO;

public class DCTO extends BaseTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private int dCNo;
	private Date dCDate;
	private CustomerMasterTO fromCustomerMaster = new CustomerMasterTO();
	private CustomerMasterTO toCustomerMaster = new CustomerMasterTO();
	private String toCustomerMobileNumber;
	private String remarks;
	private int totalNoOfQty;
	private BigDecimal amount = new BigDecimal(0);
	private String otherChargesDetails;
	private BigDecimal otherChargesAmount = new BigDecimal(0);
	private BigDecimal netAmount = new BigDecimal(0);
	private int modeOfPay;
	private VehicleMasterTO vehicleMaster;
	private EmployeeMasterTO driverName;
	private int deliveryStatus;
	private BigDecimal receivedAmount = new BigDecimal(0);
	private EmployeeMasterTO deliveredBy;
	private Date deliveredDate;
	private EmployeeMasterTO amountReceivedBy;
	private Date amountReceivedDate;
	private EmployeeMasterTO dCAmountCheckBy;
	private Date dCAmountCheckDate;
	private int rowStatus;
	private boolean officeDelivery;
	private BranchMasterTO branchMaster;
	private boolean officeDelivered;
	private Date officeDeliveredDate;
	private int officeId;
	public DCTO() {
		setRowStatus(BillingConstant.ACTIVE);
		setDeliveryStatus(BillingConstant.DELIVERY_STATUS_PENDING);
		setOtherChargesDetails("");
		setOfficeDelivery(Boolean.FALSE);
		setOfficeDelivered(Boolean.FALSE);
	}

	public int getdCNo() {
		return dCNo;
	}

	public void setdCNo(int dCNo) {
		this.dCNo = dCNo;
	}

	public Date getdCDate() {
		return dCDate;
	}

	public void setdCDate(Date dCDate) {
		this.dCDate = dCDate;
	}

	public CustomerMasterTO getFromCustomerMaster() {
		return fromCustomerMaster;
	}

	public void setFromCustomerMaster(CustomerMasterTO fromCustomerMaster) {
		this.fromCustomerMaster = fromCustomerMaster;
	}

	public CustomerMasterTO getToCustomerMaster() {
		return toCustomerMaster;
	}

	public void setToCustomerMaster(CustomerMasterTO toCustomerMaster) {
		this.toCustomerMaster = toCustomerMaster;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public int getTotalNoOfQty() {
		return totalNoOfQty;
	}

	public void setTotalNoOfQty(int totalNoOfQty) {
		this.totalNoOfQty = totalNoOfQty;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getOtherChargesDetails() {
		return otherChargesDetails;
	}

	public void setOtherChargesDetails(String otherChargesDetails) {
		this.otherChargesDetails = otherChargesDetails;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public int getModeOfPay() {
		return modeOfPay;
	}

	public void setModeOfPay(int modeOfPay) {
		this.modeOfPay = modeOfPay;
	}

	public VehicleMasterTO getVehicleMaster() {
		return vehicleMaster;
	}

	public void setVehicleMaster(VehicleMasterTO vehicleMaster) {
		this.vehicleMaster = vehicleMaster;
	}

	public EmployeeMasterTO getDriverName() {
		return driverName;
	}

	public void setDriverName(EmployeeMasterTO driverName) {
		this.driverName = driverName;
	}

	public int getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(int deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public BigDecimal getReceivedAmount() {
		return receivedAmount;
	}

	public void setReceivedAmount(BigDecimal receivedAmount) {
		this.receivedAmount = receivedAmount;
	}

	@Override
	public int getRowStatus() {
		return rowStatus;
	}
	
	@Override
	public void setRowStatus(int rowStatus) {
		this.rowStatus = rowStatus;
	}

	public BranchMasterTO getBranchMaster() {
		return branchMaster;
	}

	public void setBranchMaster(BranchMasterTO branchMaster) {
		this.branchMaster = branchMaster;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public BigDecimal getOtherChargesAmount() {
		return otherChargesAmount;
	}

	public void setOtherChargesAmount(BigDecimal otherChargesAmount) {
		this.otherChargesAmount = otherChargesAmount;
	}

	public EmployeeMasterTO getDeliveredBy() {
		return deliveredBy;
	}

	public void setDeliveredBy(EmployeeMasterTO deliveredBy) {
		this.deliveredBy = deliveredBy;
	}

	public Date getDeliveredDate() {
		return deliveredDate;
	}

	public void setDeliveredDate(Date deliveredDate) {
		this.deliveredDate = deliveredDate;
	}

	public EmployeeMasterTO getAmountReceivedBy() {
		return amountReceivedBy;
	}

	public void setAmountReceivedBy(EmployeeMasterTO amountReceivedBy) {
		this.amountReceivedBy = amountReceivedBy;
	}

	public Date getAmountReceivedDate() {
		return amountReceivedDate;
	}

	public void setAmountReceivedDate(Date amountReceivedDate) {
		this.amountReceivedDate = amountReceivedDate;
	}

	public EmployeeMasterTO getdCAmountCheckBy() {
		return dCAmountCheckBy;
	}

	public void setdCAmountCheckBy(EmployeeMasterTO dCAmountCheckBy) {
		this.dCAmountCheckBy = dCAmountCheckBy;
	}

	public Date getdCAmountCheckDate() {
		return dCAmountCheckDate;
	}

	public void setdCAmountCheckDate(Date dCAmountCheckDate) {
		this.dCAmountCheckDate = dCAmountCheckDate;
	}

	public boolean isOfficeDelivery() {
		return officeDelivery;
	}

	public void setOfficeDelivery(boolean officeDelivery) {
		this.officeDelivery = officeDelivery;
	}

	public boolean isOfficeDelivered() {
		return officeDelivered;
	}

	public void setOfficeDelivered(boolean officeDelivered) {
		this.officeDelivered = officeDelivered;
	}

	public Date getOfficeDeliveredDate() {
		return officeDeliveredDate;
	}

	public void setOfficeDeliveredDate(Date officeDeliveredDate) {
		this.officeDeliveredDate = officeDeliveredDate;
	}

	public int getOfficeId() {
		return officeId;
	}

	public void setOfficeId(int officeId) {
		this.officeId = officeId;
	}

	public String getToCustomerMobileNumber() {
		return toCustomerMobileNumber;
	}

	public void setToCustomerMobileNumber(String toCustomerMobileNumber) {
		this.toCustomerMobileNumber = toCustomerMobileNumber;
	}
	
}

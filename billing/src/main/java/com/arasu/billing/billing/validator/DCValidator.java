package com.arasu.billing.billing.validator;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.arasu.billing.billing.bean.DCBean;
import com.arasu.billing.billing.to.DCLineTO;
import com.arasu.billing.common.BillingConstant;
import com.arasu.billing.common.Messages;
import com.arasu.billing.common.paymentmode.PaymentMode;

public class DCValidator {

	public DCValidator() {
	
	}

	
	public boolean dcValidator(DCBean dCBean, Messages messages) {
		return validateItemList(dCBean) && validateAmount(dCBean,messages);
	}

	private boolean validateItemList(DCBean dCBean) {
		List<DCLineTO> dclineList = dCBean.getDcData().getDcLine().stream().filter(dcline -> dcline.getItemMaster() != null).collect(Collectors.toList());
		return !dclineList.isEmpty();
	}

	private boolean validateAmount(DCBean dCBean, Messages messages) {
		boolean validateResult = Boolean.TRUE;
		dCBean.getDcData().getDc().setReceivedAmount(new BigDecimal(0));
		if (checkPaymentMode(dCBean) && dCBean.getDcData().getDc().getAmount().compareTo(new BigDecimal(0)) == 0) {
			validateResult = Boolean.FALSE;
			messages.displayErrorMessage(null, "Error", "Total Amount is Zero!..");
		} else if (isPaidPaymentMode(dCBean)) {
			dCBean.getDcData().getDc().setAmountReceivedBy(dCBean.getCurrentInstance().getEmployeeMaster());
			dCBean.getDcData().getDc().setAmountReceivedDate(new Date());
			dCBean.getDcData().getDc().setReceivedAmount(dCBean.getDcData().getDc().getNetAmount());
		}
		return validateResult;
	}
	
	private boolean checkPaymentMode(DCBean dCBean) {
		return dCBean.getDcData().getDc().getModeOfPay() != isAccPaymentMode();
	}

	private int isAccPaymentMode() {
		return Integer
				.parseInt(PaymentMode.getModeOfPaymentList().stream().filter(pay -> pay.getLabel().equals(BillingConstant.ACC_PAYMENT_MODE)).findAny().get().getValue().toString());
	}

	private boolean isPaidPaymentMode(DCBean dCBean) {
		return dCBean.getDcData().getDc().getModeOfPay() == Integer.parseInt(
				PaymentMode.getModeOfPaymentList().stream().filter(pay -> pay.getLabel().equals(BillingConstant.PAID_PAYMENT_MODE)).findAny().get().getValue().toString());
	}
}

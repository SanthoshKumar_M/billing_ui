package com.arasu.billing.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.model.SelectItem;

public class BillingConstant {
	
	private BillingConstant() {
		
	}
	
	public static final String EMPTY_STRING = "";

	public static final char TYPE_I = 'i';
	public static final char TYPE_U = 'u';

	public static final String NEW_CUSTOMER = "New Customer Create Via DC";

	public static final Integer ACTIVE = 1;
	public static final Integer PENDING = 2;
	public static final Integer CLOSED = 3;
	public static final Integer CANCELED = 4;
	public static final Integer DELETED = 5;

	public static final String DC_STATUS_ACTIVE_STR = "ACTIVE";
	public static final String DC_STATUS_PENDING_STR = "PENDING";
	public static final String DC_STATUS_CLOSED_STR = "CLOSED";
	public static final String DC_STATUS_CANCELED_STR = "CANCELED";
	
	public static final  Integer TOPAY = 1;
	public static final Integer PAID = 2;
	public static final Integer ACC = 3;

	public static final String TOPAY_PAYMENT_MODE = "TO PAY";
	public static final String PAID_PAYMENT_MODE = "PAID";
	public static final String ACC_PAYMENT_MODE = "ACC";

	public static final Integer PAYMENT_PENDING = 1;
	public static final Integer PAYMENT_DONE = 2;

	public static final String PAYMENT_STATUS_PENDING_STR = "PENDING";
	public static final String PAYMENT_STATUS_DONE_STR = "DONE";

	public static final Integer DELIVERY_STATUS_PENDING = 1;
	public static final Integer DELIVERY_STATUS_DONE = 2;
	public static final Integer DELIVERY_STATUS_INOFFICE = 3;

	public static final String DELIVERY_STATUS_PENDING_STR = "PENDING";
	public static final String DELIVERY_STATUS_DONE_STR = "DONE";
	public static final String DELIVERY_STATUS_INOFFICE_STR = "IN OFFICE";

	protected static final Map<Integer, String> DELIVERY_STATUS_MAP = new HashMap<>();
	protected static final Map<Integer, String> DC_STATUS_MAP = new HashMap<>();
	protected static final Map<Integer, String> PAYMENT_STATUS_MAP = new HashMap<>();

	protected static final List<SelectItem> PAYMENT_STATUS_LIST = new ArrayList<>();
	protected static final List<SelectItem> DELIVERY_STATUS_LIST = new ArrayList<>();
	
	protected static final BigDecimal ZERO_AMOUNT = BigDecimal.valueOf(0.00);
	
	public static final Integer ZERO_DECIMAL_PLACE = 0;
	public static final Integer ONE_DECIMAL_PLACE = 1;
	public static final Integer TWO_DECIMAL_PLACE = 2;
	
	public static final Integer ZERO = 0;
	public static final Integer ONE = 1;
	public static final Integer TWO = 2;

	static {
		DC_STATUS_MAP.put(ACTIVE, DC_STATUS_ACTIVE_STR);
		DC_STATUS_MAP.put(PENDING, DC_STATUS_PENDING_STR);
		DC_STATUS_MAP.put(CLOSED, DC_STATUS_CLOSED_STR);
		DC_STATUS_MAP.put(CANCELED, DC_STATUS_CANCELED_STR);
		
		DELIVERY_STATUS_MAP.put(DELIVERY_STATUS_PENDING, DELIVERY_STATUS_PENDING_STR);
		DELIVERY_STATUS_MAP.put(DELIVERY_STATUS_DONE, DELIVERY_STATUS_DONE_STR);
		DELIVERY_STATUS_MAP.put(DELIVERY_STATUS_INOFFICE, DELIVERY_STATUS_INOFFICE_STR);

		PAYMENT_STATUS_MAP.put(PAYMENT_PENDING, PAYMENT_STATUS_PENDING_STR);
		PAYMENT_STATUS_MAP.put(PAYMENT_DONE, PAYMENT_STATUS_DONE_STR);

		PAYMENT_STATUS_LIST.add(new SelectItem(PAYMENT_STATUS_PENDING_STR, PAYMENT_STATUS_PENDING_STR));
		PAYMENT_STATUS_LIST.add(new SelectItem(PAYMENT_STATUS_DONE_STR, PAYMENT_STATUS_DONE_STR));
		
		DELIVERY_STATUS_LIST.add(new SelectItem(DELIVERY_STATUS_PENDING_STR, DELIVERY_STATUS_PENDING_STR));
		DELIVERY_STATUS_LIST.add(new SelectItem(DELIVERY_STATUS_DONE_STR, DELIVERY_STATUS_DONE_STR));
		
	}

	public static String getEmptyString() {
		return EMPTY_STRING;
	}

	public static char getTypeI() {
		return TYPE_I;
	}

	public static char getTypeU() {
		return TYPE_U;
	}

	public static String getNewCustomer() {
		return NEW_CUSTOMER;
	}

	public static Integer getActive() {
		return ACTIVE;
	}

	public static Integer getPending() {
		return PENDING;
	}

	public static Integer getClosed() {
		return CLOSED;
	}

	public static Integer getCanceled() {
		return CANCELED;
	}

	public static Integer getDeleted() {
		return DELETED;
	}

	public static String getDcStatusActiveStr() {
		return DC_STATUS_ACTIVE_STR;
	}

	public static String getDcStatusPendingStr() {
		return DC_STATUS_PENDING_STR;
	}

	public static String getDcStatusClosedStr() {
		return DC_STATUS_CLOSED_STR;
	}

	public static String getDcStatusCanceledStr() {
		return DC_STATUS_CANCELED_STR;
	}

	public static Integer getTopay() {
		return TOPAY;
	}

	public static Integer getPaid() {
		return PAID;
	}

	public static Integer getAcc() {
		return ACC;
	}

	public static String getTopayPaymentMode() {
		return TOPAY_PAYMENT_MODE;
	}

	public static String getPaidPaymentMode() {
		return PAID_PAYMENT_MODE;
	}

	public static String getAccPaymentMode() {
		return ACC_PAYMENT_MODE;
	}

	public static Integer getPaymentPending() {
		return PAYMENT_PENDING;
	}

	public static Integer getPaymentDone() {
		return PAYMENT_DONE;
	}

	public static String getPaymentStatusPendingStr() {
		return PAYMENT_STATUS_PENDING_STR;
	}

	public static String getPaymentStatusDoneStr() {
		return PAYMENT_STATUS_DONE_STR;
	}

	public static Integer getDeliveryStatusPending() {
		return DELIVERY_STATUS_PENDING;
	}

	public static Integer getDeliveryStatusDone() {
		return DELIVERY_STATUS_DONE;
	}

	public static String getDeliveryStatusPendingStr() {
		return DELIVERY_STATUS_PENDING_STR;
	}

	public static String getDeliveryStatusDoneStr() {
		return DELIVERY_STATUS_DONE_STR;
	}

	public static Map<Integer, String> getDeliveryStatusMap() {
		return DELIVERY_STATUS_MAP;
	}

	public static Map<Integer, String> getDcStatusMap() {
		return DC_STATUS_MAP;
	}

	public static Map<Integer, String> getPaymentStatusMap() {
		return PAYMENT_STATUS_MAP;
	}

	public static List<SelectItem> getPaymentStatusList() {
		return PAYMENT_STATUS_LIST;
	}

	public static List<SelectItem> getDeliveryStatusList() {
		return DELIVERY_STATUS_LIST;
	}

	public static BigDecimal getZeroAmount() {
		return ZERO_AMOUNT;
	}

	public static Integer getZeroDecimalPlace() {
		return ZERO_DECIMAL_PLACE;
	}

	public static Integer getOneDecimalPlace() {
		return ONE_DECIMAL_PLACE;
	}

	public static Integer getTwoDecimalPlace() {
		return TWO_DECIMAL_PLACE;
	}

	public static Integer getZero() {
		return ZERO;
	}

	public static Integer getOne() {
		return ONE;
	}

	public static Integer getTwo() {
		return TWO;
	}	
}

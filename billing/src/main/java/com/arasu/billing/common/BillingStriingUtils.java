package com.arasu.billing.common;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang3.StringUtils;

@Named
@RequestScoped
public class BillingStriingUtils {
	
	private BillingStriingUtils() {
		
	}

	public static String capitalize(String input) {
		if (StringUtils.isEmpty(input)) {
			input = "";
		}
		return WordUtils.capitalize(input);
	}

}

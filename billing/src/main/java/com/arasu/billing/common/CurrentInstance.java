package com.arasu.billing.common;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import com.arasu.billing.master.to.BranchMasterTO;
import com.arasu.billing.master.to.EmployeeMasterTO;

@Named
@RequestScoped
public class CurrentInstance implements Serializable{

	private static final long serialVersionUID = 1L;

	public FacesContext getCurrentInstance() {
		return FacesContext.getCurrentInstance();
	}

	public HttpSession getHttpSession() {
		return (HttpSession) getCurrentInstance().getExternalContext().getSession(true);
	}

	private UserSession getLoginUserDetails() {
		return (UserSession) getHttpSession().getAttribute("userobject");
	}

	public EmployeeMasterTO getLoginEmployeeDetails() {
		return getLoginUserDetails().getLoginTO().getEmployeeMaster();
	}

	public BranchMasterTO getLoginBranchDetails() {
		return getLoginUserDetails().getBranchMasterTO();
	}

	public int getLoginUserID() {
		return getLoginEmployeeDetails().getId();
	}
	
	public EmployeeMasterTO getEmployeeMaster() {
		EmployeeMasterTO employeeMaster = new EmployeeMasterTO();
		employeeMaster.setId(getLoginUserID());
		return employeeMaster;
	}

}

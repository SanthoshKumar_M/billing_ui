package com.arasu.billing.common;

import java.util.HashMap;
import java.util.Map;

import com.arasu.billing.master.to.CustomerMasterTO;

public class CustomerSuggestionDetails {
	protected static Map<String, CustomerMasterTO> customerSuggestionNameDetails = new HashMap<>();

	private CustomerSuggestionDetails() {
	}

	public static Map<String, CustomerMasterTO> getCustomerSuggestionNameDetails() {
		return customerSuggestionNameDetails;
	}

	public static void setCustomerSuggestionNameDetails(Map<String, CustomerMasterTO> customerSuggestionNameDetails) {
		CustomerSuggestionDetails.customerSuggestionNameDetails = customerSuggestionNameDetails;
	}

}

package com.arasu.billing.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class DateUtil {
	DateFormat dateMonthYearDateFormat = new SimpleDateFormat("dd-MM-yyyy");
	static DateFormat MONTH_YEAR = new SimpleDateFormat("MMM-yyyy");
	public String dateToStringConverter(Date date) {
		return dateMonthYearDateFormat.format(date);
	}

	public Date getCurrentDate() {
		return new Date();
	}

	public static Date getLastDateOfMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		return cal.getTime();
	}
	
	public static String getMonthYear(Date date) {
		return MONTH_YEAR.format(date);
	}

}

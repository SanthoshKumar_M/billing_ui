package com.arasu.billing.common;

import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.primefaces.util.ComponentTraversalUtils;

@Named("p")
@RequestScoped
public class FindClientID {
	
	public String component(String clientId) {
		StringBuilder clientIds = new StringBuilder();
		FacesContext facesContext = FacesContext.getCurrentInstance();

		String[] clientStrings = clientId.split("\\s+");
		for (int i = 0; i < clientStrings.length; i++) {
			if (clientIds.length() > 0) {
				clientIds.append(" ");
			}
			UIComponent component = ComponentTraversalUtils.firstWithId(clientStrings[i], facesContext.getViewRoot());
			clientIds.append(component.getClientId(facesContext));

		}
		return clientIds.toString();
	}
}

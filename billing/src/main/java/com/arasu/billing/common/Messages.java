package com.arasu.billing.common;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class Messages {
	private CurrentInstance currentInstance;

	public Messages() {

	}

	@Inject
	public Messages(final CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	public void displayInfoMessage(String clientId, String message, String details) {
		currentInstance.getCurrentInstance().addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_INFO, message, details));
	}

	public void displayErrorMessage(String clientId, String message, String details) {
		currentInstance.getCurrentInstance().addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_FATAL, message, details));
	}

}

package com.arasu.billing.common;

import javax.inject.Inject;

import org.primefaces.PrimeFaces;



public class PrimeFacesUtil {
	@Inject FindClientID findClientID; 
	
	protected void primefacesUpdate(String idComponente) {
           PrimeFaces.current().ajax().update(findClientID.component(idComponente));
        }

	public void executeJS(String jsFunctionName) {
		 PrimeFaces.current().executeScript(jsFunctionName+"return false;");
	}
	
	public void select(String componentId) {
		PrimeFaces.current().focus(findClientID.component(componentId));
		
	}
  
}

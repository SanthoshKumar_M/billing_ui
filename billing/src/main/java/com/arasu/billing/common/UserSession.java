package com.arasu.billing.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import com.arasu.billing.admin.to.LoginTO;
import com.arasu.billing.master.to.BranchMasterTO;

@SessionScoped
@Named
public class UserSession implements Serializable {
	private static final long serialVersionUID = 1L;

	private LoginTO loginTO = new LoginTO();
	private BranchMasterTO branchMasterTO;
	private List<BranchMasterTO> branchMasterTOs = new ArrayList<>();
	private String employeePhoto;

	public LoginTO getLoginTO() {
		return loginTO;
	}

	public void setLoginTO(LoginTO loginTO) {
		this.loginTO = loginTO;
	}

	public BranchMasterTO getBranchMasterTO() {
		return branchMasterTO;
	}

	public void setBranchMasterTO(BranchMasterTO branchMasterTO) {
		this.branchMasterTO = branchMasterTO;
	}

	public List<BranchMasterTO> getBranchMasterTOs() {
		return branchMasterTOs;
	}

	public void setBranchMasterTOs(List<BranchMasterTO> branchMasterTOs) {
		this.branchMasterTOs = branchMasterTOs;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getEmployeePhoto() {
		return employeePhoto;
	}

	public void setEmployeePhoto(String employeePhoto) {
		this.employeePhoto = employeePhoto;
	}

}

package com.arasu.billing.common.paymentmode;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import com.arasu.billing.common.BillingConstant;

public class PaymentMode {
	private static List<SelectItem> modeOfPaymentList = new ArrayList<>();

	private PaymentMode() {
	}

	public static void init() {
		modeOfPaymentList.add(new SelectItem(BillingConstant.TOPAY, BillingConstant.TOPAY_PAYMENT_MODE));
		modeOfPaymentList.add(new SelectItem(BillingConstant.PAID, BillingConstant.PAID_PAYMENT_MODE));
		modeOfPaymentList.add(new SelectItem(BillingConstant.ACC, BillingConstant.ACC_PAYMENT_MODE));
	}

	public static List<SelectItem> getModeOfPaymentList() {
		if (modeOfPaymentList.isEmpty()) {
			init();
		}
		return modeOfPaymentList;
	}

	public static void setModeOfPaymentList(List<SelectItem> modeOfPaymentList) {
		PaymentMode.modeOfPaymentList = modeOfPaymentList;
	}

}

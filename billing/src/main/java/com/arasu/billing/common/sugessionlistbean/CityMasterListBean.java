package com.arasu.billing.common.sugessionlistbean;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import com.arasu.billing.master.to.CityMasterTO;

@Named
@ApplicationScoped
public class CityMasterListBean {

	private static List<CityMasterTO> citySuggestionList = new ArrayList<>();

	private CityMasterListBean() {
		
	}

	public static List<CityMasterTO> getCitySuggestionList() {
		return citySuggestionList;
	}

	public static void setCitySuggestionList(List<CityMasterTO> citySuggestionList) {
		CityMasterListBean.citySuggestionList = citySuggestionList;
	}

}

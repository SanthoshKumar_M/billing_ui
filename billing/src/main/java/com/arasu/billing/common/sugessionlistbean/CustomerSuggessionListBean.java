package com.arasu.billing.common.sugessionlistbean;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import com.arasu.billing.master.to.CustomerMasterTO;

@Named
@ApplicationScoped
public class CustomerSuggessionListBean {

	private static List<CustomerMasterTO> customerSuggestionList = new ArrayList<>();

	private CustomerSuggessionListBean() {

	}

	public static List<CustomerMasterTO> getCustomerSuggestionList() {
		return customerSuggestionList;
	}

	public static void setCustomerSuggestionList(List<CustomerMasterTO> customerSuggestionList) {
		CustomerSuggessionListBean.customerSuggestionList = customerSuggestionList;
	}

}

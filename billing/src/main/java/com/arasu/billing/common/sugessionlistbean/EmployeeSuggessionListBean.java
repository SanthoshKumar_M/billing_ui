package com.arasu.billing.common.sugessionlistbean;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import com.arasu.billing.master.to.EmployeeMasterTO;

@Named
@ApplicationScoped
public class EmployeeSuggessionListBean {
	private static List<EmployeeMasterTO> employeeSuggestionList = new ArrayList<>();

	private EmployeeSuggessionListBean() {
		
	}

	public static List<EmployeeMasterTO> getEmployeeSuggestionList() {
		return employeeSuggestionList;
	}

	public static void setEmployeeSuggestionList(List<EmployeeMasterTO> employeeSuggestionList) {
		EmployeeSuggessionListBean.employeeSuggestionList = employeeSuggestionList;
	}

}

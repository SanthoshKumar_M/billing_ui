package com.arasu.billing.common.sugessionlistbean;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import com.arasu.billing.master.to.ItemMasterTO;

@Named
@ApplicationScoped
public class ItemSuggessionListBean {

	private static List<ItemMasterTO> itemSuggestionList = new ArrayList<>();

	private ItemSuggessionListBean() {
		
	}

	public static List<ItemMasterTO> getItemSuggestionList() {
		return itemSuggestionList;
	}

	public static void setItemSuggestionList(List<ItemMasterTO> itemSuggestionList) {
		ItemSuggessionListBean.itemSuggestionList = itemSuggestionList;
	}

}

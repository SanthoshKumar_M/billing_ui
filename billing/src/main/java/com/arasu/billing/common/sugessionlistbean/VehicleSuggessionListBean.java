package com.arasu.billing.common.sugessionlistbean;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import com.arasu.billing.master.to.VehicleMasterTO;

@Named
@ApplicationScoped
public class VehicleSuggessionListBean {

	private static List<VehicleMasterTO> vehicleMasterTOs = new ArrayList<>();

	private VehicleSuggessionListBean() {
		
	}

	public static List<VehicleMasterTO> getVehicleMasterTOs() {
		return vehicleMasterTOs;
	}

	public static void setVehicleMasterTOs(List<VehicleMasterTO> vehicleMasterTOs) {
		VehicleSuggessionListBean.vehicleMasterTOs = vehicleMasterTOs;
	}
}

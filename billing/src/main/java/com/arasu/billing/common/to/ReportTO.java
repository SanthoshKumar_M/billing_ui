package com.arasu.billing.common.to;

import java.io.Serializable;
import java.math.BigDecimal;

public class ReportTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private BigDecimal qty;
	private String itemName;

	public BigDecimal getQty() {
		return qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

}

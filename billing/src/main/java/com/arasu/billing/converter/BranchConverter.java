package com.arasu.billing.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.admin.bean.LoginBean;
import com.arasu.billing.master.to.BranchMasterTO;

@Named
@FacesConverter(value = "branchConverter", managed = true)
public class BranchConverter implements Converter {

	@Inject
	private LoginBean loginBean;

	@Override
	public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
		if (loginBean != null && value != null && value.trim().length() > 0) {
			try {
				return loginBean.getBranchMasterTOs().stream().filter(branch -> branch.getId() == (Integer.parseInt(value))).findFirst().orElse(null);
			} catch (NumberFormatException e) {
				throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid Branch."));
			}
		} else {
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent uic, Object object) {
		if (object != null) {
			return String.valueOf(((BranchMasterTO) object).getId());
		} else {
			return null;
		}
	}

	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}

}

package com.arasu.billing.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.common.sugessionlistbean.CityMasterListBean;
import com.arasu.billing.master.service.CityMasterService;
import com.arasu.billing.master.to.CityMasterTO;

@Named
@FacesConverter(value = "cityConverter", managed = true)
public class CityConverter implements Converter {
	@Inject
	private CityMasterService cityMasterService;

	@Override
	public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
		if (value != null && value.trim().length() > 0 && Integer.parseInt(value) > 0) {
			try {
				if (CityMasterListBean.getCitySuggestionList().isEmpty()) {
					cityMasterService.getAllCitysList();
				}
				return CityMasterListBean.getCitySuggestionList().stream().filter(branch -> branch.getId() == (Integer.parseInt(value))).findFirst().orElse(null);
			} catch (NumberFormatException e) {
				throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid City."));
			}
		} else {
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent uic, Object object) {
		if (object != null) {
			return String.valueOf(((CityMasterTO) object).getId());
		} else {
			return null;
		}
	}

}

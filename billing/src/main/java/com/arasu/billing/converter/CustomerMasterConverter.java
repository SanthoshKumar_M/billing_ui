package com.arasu.billing.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;

import com.arasu.billing.common.sugessionlistbean.CustomerSuggessionListBean;
import com.arasu.billing.master.service.CustomerMasterService;
import com.arasu.billing.master.to.CustomerMasterTO;

@Named
@FacesConverter(value = "customerMasterConverter", managed = true)
public class CustomerMasterConverter implements Converter {

	@Inject
	private CustomerMasterService customerMasterService;

	@Override
	public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
		if (value != null && value.trim().length() > 0 && checkValueIsInteger(value) && Integer.parseInt(value) > 0) {
			try {
				if (CustomerSuggessionListBean.getCustomerSuggestionList().isEmpty()) {
					customerMasterService.getAllCustomerList();
				}
				return CustomerSuggessionListBean.getCustomerSuggestionList().stream().filter(emptype -> emptype.getId() == (Integer.parseInt(value))).findFirst().orElse(null);
			} catch (NumberFormatException e) {
				throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid Customer "));
			}
		} else {
			try {
				return CustomerSuggessionListBean.getCustomerSuggestionList().stream().filter(emptype -> emptype.getCustomerPrintName().equals(value)).findFirst().orElse(null);
			} catch (NumberFormatException e) {
				throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid Customer "));
			}
		}
	}

	private boolean checkValueIsInteger(String value) {
		return StringUtils.isNumeric(value);
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent uic, Object object) {
		if (object != null) {
			int id = ((CustomerMasterTO) object).getId();
			if (id > 0) {
				return String.valueOf(id);
			} else {
				return ((CustomerMasterTO) object).getCustomerPrintName();
			}
		} else {
			return null;
		}
	}

}

package com.arasu.billing.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;

import com.arasu.billing.common.sugessionlistbean.EmployeeSuggessionListBean;
import com.arasu.billing.master.to.EmployeeMasterTO;

@Named
@FacesConverter(value = "employeeMasterConverter", managed = true)
public class EmployeeMasterConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
		if (value != null && value.trim().length() > 0) {
			try {
				return EmployeeSuggessionListBean.getEmployeeSuggestionList().stream().filter(empmaster -> empmaster.getId() == (Integer.parseInt(value))).findFirst().orElse(null);
			} catch (NumberFormatException e) {
				throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid Employee."));
			}
		} else {
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent uic, Object object) {
		if (object != null) {
			return String.valueOf(((EmployeeMasterTO) object).getId());
		} else {
			return null;
		}
	}

}

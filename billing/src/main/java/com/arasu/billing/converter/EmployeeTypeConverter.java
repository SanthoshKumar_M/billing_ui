package com.arasu.billing.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.master.bean.EmployeeMasterBean;
import com.arasu.billing.master.to.EmployeeTypeMasterTO;

@Named
@FacesConverter(value = "employeeTypeConverter", managed = true)
public class EmployeeTypeConverter implements Converter {

	@Inject
	private EmployeeMasterBean employeeMasterBean;

	@Override
	public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
		if (employeeMasterBean != null && value != null && value.trim().length() > 0) {
			try {
				return employeeMasterBean.getEmployeeTypeTOs().stream().filter(emptype -> emptype.getId() == (Integer.parseInt(value))).findFirst().orElse(null);
			} catch (NumberFormatException e) {
				throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid Employee Type."));
			}
		} else {
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent uic, Object object) {
		if (object != null) {
			return String.valueOf(((EmployeeTypeMasterTO) object).getId());
		} else {
			return null;
		}
	}

	public EmployeeMasterBean getEmployeeMasterBean() {
		return employeeMasterBean;
	}

	public void setEmployeeMasterBean(EmployeeMasterBean employeeMasterBean) {
		this.employeeMasterBean = employeeMasterBean;
	}

}

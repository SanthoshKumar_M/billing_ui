package com.arasu.billing.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.common.sugessionlistbean.ItemSuggessionListBean;
import com.arasu.billing.master.service.ItemMasterService;
import com.arasu.billing.master.to.ItemMasterTO;

@Named
@FacesConverter(value = "ItemMasterConverter", managed = true)
public class ItemMasterConverter implements Converter {
	@Inject
	private ItemMasterService itemMasterService;

	@Override
	public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
		if (value != null && value.trim().length() > 0) {
			try {
				if (ItemSuggessionListBean.getItemSuggestionList().isEmpty()) {
					itemMasterService.getAllItemDetails();
				}
				return ItemSuggessionListBean.getItemSuggestionList().stream().filter(item -> item.getId() == (Integer.parseInt(value))).findFirst().orElse(null);
			} catch (NumberFormatException e) {
				throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid Item Type."));
			}
		} else {
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent uic, Object object) {
		if (object != null) {
			return String.valueOf(((ItemMasterTO) object).getId());
		} else {
			return null;
		}
	}

}

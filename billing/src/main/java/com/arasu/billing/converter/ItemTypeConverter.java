package com.arasu.billing.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.master.bean.ItemMasterBean;
import com.arasu.billing.master.to.ItemTypeMasterTO;

@Named
@FacesConverter(value = "itemTypeConverter", managed = true)
public class ItemTypeConverter implements Converter {

	@Inject
	private ItemMasterBean itemMasterBean;

	@Override
	public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
		if (itemMasterBean != null && value != null && value.trim().length() > 0) {
			try {
				return itemMasterBean.getItemTypeMasterTOs().stream().filter(itemtype -> itemtype.getId() == (Integer.parseInt(value))).findFirst().orElse(null);
			} catch (NumberFormatException e) {
				throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid Employee Type."));
			}
		} else {
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent uic, Object object) {
		if (object != null) {
			return String.valueOf(((ItemTypeMasterTO) object).getId());
		} else {
			return null;
		}
	}

}

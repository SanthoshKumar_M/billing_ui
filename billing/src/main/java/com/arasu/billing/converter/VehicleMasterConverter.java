package com.arasu.billing.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.common.sugessionlistbean.VehicleSuggessionListBean;
import com.arasu.billing.master.service.VehicleMasterService;
import com.arasu.billing.master.to.VehicleMasterTO;

@Named
@FacesConverter(value = "VehicleMasterConverter", managed = true)
public class VehicleMasterConverter implements Converter {
	@Inject
	private VehicleMasterService vehicleMasterService;

	@Override
	public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
		if (value != null && value.trim().length() > 0) {
			try {
				if (VehicleSuggessionListBean.getVehicleMasterTOs().isEmpty()) {
					vehicleMasterService.getAllVehicleDetails();
				}
				return VehicleSuggessionListBean.getVehicleMasterTOs().stream().filter(vehicle -> vehicle.getId() == (Integer.parseInt(value))).findFirst().orElse(null);
			} catch (NumberFormatException e) {
				throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid Vehicle."));
			}
		} else {
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent uic, Object object) {
		if (object != null) {
			return String.valueOf(((VehicleMasterTO) object).getId());
		} else {
			return null;
		}
	}

}

package com.arasu.billing.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.arasu.billing.billing.to.DCLineTO;
import com.arasu.billing.billing.to.DCTO;
import com.arasu.billing.common.to.ReportTO;

/**
 * @author SAN
 *
 */
public class DCData implements Serializable {
	private static final long serialVersionUID = 1L;

	private DCTO dc = new DCTO();
	private List<DCLineTO> dcLine = new ArrayList<>();
	private List<Integer> removeItemIds = new ArrayList<>();
	private List<ReportTO> reportTOs = new ArrayList<>();
	private transient Map<String, Object> parameters = new HashMap<>();
	
	public DCTO getDc() {
		return dc;
	}

	public void setDc(DCTO dc) {
		this.dc = dc;
	}

	public List<DCLineTO> getDcLine() {
		return dcLine;
	}

	public void setDcLine(List<DCLineTO> dcLine) {
		this.dcLine = dcLine;
	}

	public List<Integer> getRemoveItemIds() {
		return removeItemIds;
	}

	public void setRemoveItemIds(List<Integer> removeItemIds) {
		this.removeItemIds = removeItemIds;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<ReportTO> getReportTOs() {
		return reportTOs;
	}

	public void setReportTOs(List<ReportTO> reportTOs) {
		this.reportTOs = reportTOs;
	}

	public Map<String, Object> getParameters() {
		return parameters;
	}

	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}
}

package com.arasu.billing.data;

import java.io.Serializable;

import com.arasu.billing.admin.to.LoginTO;
import com.arasu.billing.master.to.EmployeeMasterTO;

public class EmployeeData implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private EmployeeMasterTO employeeMaster;
	private LoginTO login;

	public EmployeeMasterTO getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMasterTO employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public LoginTO getLogin() {
		return login;
	}

	public void setLogin(LoginTO login) {
		this.login = login;
	}

}

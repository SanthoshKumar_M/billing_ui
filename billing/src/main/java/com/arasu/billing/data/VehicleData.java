package com.arasu.billing.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.arasu.billing.master.to.VehicleInsuranceDetailsMasterTO;
import com.arasu.billing.master.to.VehicleMasterTO;

public class VehicleData implements Serializable {
	private static final long serialVersionUID = 1L;
	private VehicleMasterTO vehicleMaster = new VehicleMasterTO();
	private List<VehicleInsuranceDetailsMasterTO> insuranceDetails = new ArrayList<>();

	public VehicleMasterTO getVehicleMaster() {
		return vehicleMaster;
	}

	public void setVehicleMaster(VehicleMasterTO vehicleMaster) {
		this.vehicleMaster = vehicleMaster;
	}

	public List<VehicleInsuranceDetailsMasterTO> getInsuranceDetails() {
		return insuranceDetails;
	}

	public void setInsuranceDetails(List<VehicleInsuranceDetailsMasterTO> insuranceDetails) {
		this.insuranceDetails = insuranceDetails;
	}

}

package com.arasu.billing.extension;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.common.CurrentInstance;

@RequestScoped
@Named
public class StoredDataBean implements Serializable {

//	private static final long serialVersionUID = 1L;
//
//	private static String userDataLoc = System.getProperty("catalina.base") + "/webapps/" + "appdata" + File.separator;;
//	private static String sessionDataLoc = userDataLoc + "sessionData" + File.separator;
//	private static SimpleDateFormat userDF = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
//	private List<String> storedData;
//	private String stateName;
//	private CurrentInstance currentInstance;
//
//	private static String parkingCode;
//	private static String autoSavePrefix;
//	private static String defaultSavePrefix;
//	private static String defaultFAILPrefix;
//	private static String contextDataLoc = null;
//
//	@Inject
//	public StoredDataBean(final CurrentInstance currentInstance) {
//		this.currentInstance = currentInstance;
//	}
//
//	@PostConstruct
//	public void init() {
//		new File(sessionDataLoc).mkdirs();
//		stateName = "";
//		parkingCode = "_pks";
//		autoSavePrefix = "AUTOSAVE_";
//		defaultSavePrefix = "SAVE_";
//		defaultFAILPrefix = "FAILED";
//		contextDataLoc = "../../appdata/previewData/";
//		storedData = new ArrayList<String>();
//	}
//
//	public void loadStoredDataList(int employeeId, String voucherId) {
//		this.storedData = getSavedStates(employeeId, voucherId);
//	}
//
//	@SuppressWarnings("unchecked")
//	public synchronized static <T> List<String> getSavedStates(int employeeId, String voucherId) {
//		HashMap<String, HashMap<String, T>> dataMap = new HashMap<String, HashMap<String, T>>();
//		HashMap<String, T> parkingMap = new HashMap<String, T>();
//		String empId = employeeId + "", fileName = "";
//		ObjectOutputStream oos = null;
//		ObjectInputStream ois = null;
//		Object stateObj = null;
//		List<String> stateNameList = new ArrayList<String>();
//		for (int pos = 0; pos < empId.length(); pos++) {
//			fileName += ((int) empId.charAt(pos));
//		}
//		File fileData = null;
//		try {
//			File dir = new File(sessionDataLoc + "");
//			if (dir.exists()) {
//				fileData = new File(sessionDataLoc + File.separator + fileName + parkingCode + ".sav");
//				FileInputStream fis = new FileInputStream(fileData);
//				ois = new ObjectInputStream(new BufferedInputStream(fis));
//				stateObj = ois.readObject();
//
//				if (stateObj != null) {
//					dataMap = (HashMap<String, HashMap<String, T>>) stateObj;
//					if (dataMap.containsKey(voucherId)) {
//						parkingMap = dataMap.get(voucherId);
//						stateNameList.addAll(parkingMap.keySet());
//					}
//					if (dataMap.containsKey(autoSavePrefix + voucherId)) {
//						parkingMap = dataMap.get(autoSavePrefix + voucherId);
//						stateNameList.addAll(parkingMap.keySet());
//					}
//				}
//
//			}
//		} catch (InvalidClassException invalidClass) {
//			fileData.delete();
//		} catch (EOFException e) {
//			fileData.delete();
//		} catch (ClassNotFoundException | IOException e) {
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			try {
//				if (ois != null) {
//					ois.close();
//				}
//				if (oos != null) {
//					oos.close();
//				}
//
//			} catch (IOException e) {
//			}
//		}
//		return stateNameList;
//	}
//
//	public synchronized <T extends Serializable> void parkSession(int employeeId, int voucherId, T obj, String name) {
//		if (name == null || name.isEmpty())
//			name = stateName;
//		HashMap<String, HashMap<String, T>> dataMap = null;
//		HashMap<String, T> parkingMap = null;
//		String empId = employeeId + "", fileName = "";
//		String primaryKey = "";
//		String secKey = "";
//		primaryKey = (name.equals("autosave") == true ? autoSavePrefix : "") + voucherId;
//		if (name == null || name.isEmpty())
//			secKey = defaultSavePrefix + userDF.format(new Date());
//		else if (name.equals("autosave")) {
//			secKey += autoSavePrefix + userDF.format(new Date());
//		} else if (name.equals("_FAILED")) {
//			secKey = defaultFAILPrefix + "_" + userDF.format(new Date());
//			;
//		} else {
//			secKey = name;
//		}
//
//		FileInputStream fis = null;
//		ObjectOutputStream oos = null;
//		ObjectInputStream ois = null;
//		Object stateObj = null;
//		File fileData = null;
//		for (int pos = 0; pos < empId.length(); pos++)
//			fileName += ((int) empId.charAt(pos));
//		try {
//			File dir = new File(sessionDataLoc + "");
//			if (!dir.exists())
//				dir.mkdirs();
//			fileData = new File(sessionDataLoc + File.separator + fileName + parkingCode + ".sav");
//
//			if (fileData.exists()) {
//				fis = new FileInputStream(fileData);
//				ois = new ObjectInputStream(new BufferedInputStream(fis));
//				stateObj = ois.readObject();
//
//			}
//			if (stateObj != null) {
//				dataMap = (HashMap<String, HashMap<String, T>>) stateObj;
//				if (dataMap.containsKey(primaryKey)) {
//					parkingMap = dataMap.get(primaryKey);
//					if (name.equals("autosave")) {
//						parkingMap.clear();
//					}
//					parkingMap.put(secKey, obj);
//					dataMap.put(primaryKey, parkingMap);
//				} else {
//					parkingMap = new HashMap<String, T>();
//					parkingMap.put(secKey, obj);
//					dataMap.put(primaryKey, parkingMap);
//				}
//			} else {
//				dataMap = new HashMap<String, HashMap<String, T>>();
//				parkingMap = new HashMap<String, T>();
//				parkingMap.put(secKey, obj);
//				dataMap.put(primaryKey, parkingMap);
//			}
//			FileOutputStream fos = new FileOutputStream(fileData);
//			oos = new ObjectOutputStream(new BufferedOutputStream(fos));
//			oos.writeObject(dataMap);
//			if (name.equals("autosave")) {
//				getParkedSessions().clear();
//				if (dataMap.containsKey(voucherId + "")) {
//					parkingMap = dataMap.get(voucherId + "");
//					getParkedSessions().addAll(parkingMap.keySet());
//				}
//				if (dataMap.containsKey(autoSavePrefix + voucherId)) {
//					parkingMap = dataMap.get(autoSavePrefix + voucherId);
//					getParkedSessions().addAll(parkingMap.keySet());
//				}
//			} else {
//				getParkedSessions().add(secKey);
//			}
//		} catch (InvalidClassException invalidClass) {
//			/**
//			 * To avoid blocking of the action due to exception Recreating instead of
//			 * clearing to avoid opening an extra stream
//			 **/
//			fileData.delete();
//		} catch (EOFException e) {
//			fileData.delete();
//		} catch (IOException | ClassNotFoundException e) {
//			System.out.println("Exception in parkSession " + e);
//		} catch (Exception e) {
//			System.out.println("UNExpected Exception in parkSession " + e);
//		} finally {
//			try {
//				if (oos != null) {
//					oos.close();
//				}
//				if (ois != null) {
//					ois.close();
//				}
//			} catch (IOException e) {
//			}
//		}
//
//	}
//
//	@SuppressWarnings({ "unused", "rawtypes", "unchecked" })
//	public void removeSelectedState(int employeeId, int voucherId, String name) {
//		FacesContext context = FacesContext.getCurrentInstance();
//		Map<String, String> map = context.getExternalContext().getRequestParameterMap();
//		stateName = map.get("removeState");
//
//		if (name == null || name.isEmpty())
//			name = stateName;
//		HashMap<String, HashMap> dataMap = new HashMap<String, HashMap>();
//		HashMap parkingMap = new HashMap();
//		String empId = employeeId + "", fileName = "";
//		FileInputStream fis = null;
//		FileOutputStream fos = null;
//		ObjectOutputStream oos = null;
//		ObjectInputStream ois = null;
//		Object stateObj = null;
//		List<String> stateNameList = new ArrayList<String>();
//		for (int pos = 0; pos < empId.length(); pos++)
//			fileName += ((int) empId.charAt(pos));
//		try {
//			File dir = new File(sessionDataLoc + "");
//			if (dir.exists()) {
//				try {
//					fis = new FileInputStream(sessionDataLoc + File.separator + fileName + parkingCode + ".sav");
//					ois = new ObjectInputStream(new BufferedInputStream(fis));
//					try {
//						stateObj = ois.readObject();
//					} catch (EOFException e) {
//						if (ois != null)
//							ois.close();
//					}
//					if (stateObj != null) {
//						dataMap = (HashMap<String, HashMap>) stateObj;
//						if (dataMap.containsKey(voucherId + "") && dataMap.get(voucherId + "").containsKey(name)) {
//							parkingMap = dataMap.get(voucherId + "");
//						} else if (dataMap.containsKey(autoSavePrefix + voucherId)
//								&& dataMap.get(autoSavePrefix + voucherId).containsKey(name)) {
//							parkingMap = dataMap.get(autoSavePrefix + voucherId);
//						} else {
//							return;
//						}
//						parkingMap.remove(name);
//						fos = new FileOutputStream(sessionDataLoc + File.separator + fileName + parkingCode + ".sav");
//						oos = new ObjectOutputStream(fos);
//						oos.writeObject(dataMap);
//						this.getParkedSessions().remove(name);
//					}
//				} catch (ClassNotFoundException | IOException e) {
//				} finally {
//					try {
//						if (ois != null)
//							ois.close();
//						if (oos != null) {
//							oos.close();
//						}
//					} catch (IOException e) {
//					}
//				}
//			}
//		} catch (Exception e) {
//		}
//
//	}
//
//	@SuppressWarnings({ "unchecked", "static-access", "rawtypes", "unused" })
//	public <T extends Serializable> void resumeState(int employeeId, int voucherId, T currentState,
//			String selectedStateName, String transactionIdField, String restoreListener) {
//		T previousState = null;
//		HashMap<String, HashMap<String, T>> dataMap = new HashMap<String, HashMap<String, T>>();
//		HashMap<String, T> parkingMap = new HashMap<String, T>();
//		FileOutputStream fos = null;
//		ObjectOutputStream oos = null;
//		ObjectInputStream ois = null;
//		Object stateObj = null;
//		T savedState = null;
//		String empId = employeeId + "", fileName = "";
//		FacesContext context = FacesContext.getCurrentInstance();
//		Map<String, String> map = context.getExternalContext().getRequestParameterMap();
//		stateName = map.get("selectedState");
//
//		if (selectedStateName == null || selectedStateName.isEmpty()) {
//			if (this.stateName != null && !this.stateName.isEmpty()) {
//				selectedStateName = this.stateName;
//			} else {
//				return;
//			}
//		}
//
//		for (int pos = 0; pos < empId.length(); pos++) {
//			fileName += ((int) empId.charAt(pos));
//		}
//		File dir = new File(sessionDataLoc + "");
//		if (dir.exists()) {
//			try {
//				FileInputStream fis = new FileInputStream(
//						sessionDataLoc + File.separator + fileName + parkingCode + ".sav");
//				ois = new ObjectInputStream(new BufferedInputStream(fis));
//				try {
//					stateObj = ois.readObject();
//				} catch (EOFException | ClassNotFoundException e) {
//					if (ois != null)
//						ois.close();
//					return;
//				}
//				dataMap = (HashMap<String, HashMap<String, T>>) stateObj;
//				if (dataMap.containsKey(voucherId + "") && dataMap.get(voucherId + "").containsKey(selectedStateName)) {
//					parkingMap = dataMap.get(voucherId + "");
//				} else if (dataMap.containsKey(autoSavePrefix + voucherId)
//						&& dataMap.get(autoSavePrefix + voucherId).containsKey(selectedStateName)) {
//					parkingMap = dataMap.get(autoSavePrefix + voucherId);
//				} else {
//					return;
//				}
//				previousState = parkingMap.get(selectedStateName);
//				Class cState = currentState.getClass();
//				Class type = null;
//
////				try {
//					/** Testing if the Id field Exists is possible **/
//
//					// cState.getMethod("set" +
//					// idField,type).invoke(currentState,"wer");
//					resumePreviousState(currentState, previousState, selectedStateName);
//
//					/**
//					 * NA - Not Applicable
//					 * 
//					 * Useful when transaction ID is not applicable for transaction
//					 * 
//					 */
////					if (!transactionIdField.equals("NA")) {
////
////						for (Field fld : cState.getDeclaredFields()) {
////							if (transactionIdField.equals(fld.getName()))
////								type = fld.getType();
////						}
////
//////						if (type != null) {
//////							String idField = transactionIdField.substring(0, 1).toUpperCase()
//////									+ transactionIdField.substring(1);
//////							cState.getMethod("set" + idField, type).invoke(currentState, getAvailableTxn(voucherId));
//////						} else {
////						cState.getMethod(transactionIdField).invoke(currentState);
//////						}
////
////						if (restoreListener != null && !restoreListener.isEmpty())
////							cState.getMethod(restoreListener).invoke(currentState);
////					}
//
////				} catch (IllegalAccessException | IllegalArgumentException | SecurityException
////						| InvocationTargetException | NoSuchMethodException e) {
////					e.printStackTrace();
////					return;
////				}
//				if (!selectedStateName.contains(this.defaultFAILPrefix)) {
//					this.getParkedSessions().remove(selectedStateName);
//					parkingMap.remove(selectedStateName);
//				}
//				fos = new FileOutputStream(sessionDataLoc + File.separator + fileName + parkingCode + ".sav");
//				oos = new ObjectOutputStream(fos);
//				oos.writeObject(dataMap);
//			} catch (IOException exp) {
//				exp.printStackTrace();
//			} catch (IllegalArgumentException e) {
//				e.printStackTrace();
//			} catch (SecurityException e) {
//				e.printStackTrace();
//			} finally {
//				try {
//					if (ois != null)
//						ois.close();
//					if (oos != null)
//						oos.close();
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//
//	}
//
//	@SuppressWarnings({ "unchecked", "unused", "rawtypes" })
//	public static <T extends Serializable> void resumePreviousState(T currentState, T previousState,
//			String selectedStateName) {
//		if (selectedStateName == null | selectedStateName.isEmpty())
//			return;
//
//		String stateInfo = "";
//		Class cState = currentState.getClass();
//		Field[] fields = cState.getDeclaredFields();
//		for (Field fld : fields) {
//			String fName = fld.getName().toLowerCase();
//			if (fName.contains("report") || fName.contains("qry") || fName.contains("pdf")) {
//				continue;
//			}
//			String name = fld.getName().substring(0, 1).toUpperCase() + fld.getName().substring(1);
//
//			try {
//				/** Assigning current state to login field */
////				if (fld.getType().equals(CurrentInstance.class)) {
////					cState.getMethod("set" + name, fld.getType()).invoke(currentState, currentInstance);
////				} else {
//				cState.getMethod("set" + name, fld.getType()).invoke(currentState, cState
//						.getMethod(((fld.getType() == boolean.class) ? "is" : "get") + name).invoke(previousState));
////				}
//			} catch (NoSuchMethodException | InvocationTargetException | NullPointerException exp) {
//				continue;
//			} catch (IllegalAccessException | IllegalArgumentException | SecurityException e) {
//				return;
//			}
//		}
//
//		// if(cState.getMethod())
//	}
//	
//	public void nameSelect() {
//		System.out.println("Inside Method");
//	}
//
//	public List<String> getStoredData() {
//		return storedData;
//	}
//
//	public void setStoredData(List<String> storedData) {
//		this.storedData = storedData;
//	}
//
//	public List<String> getParkedSessions() {
//		return storedData;
//	}
//
//	public void setParkedSessions(List<String> parkingSessions) {
//		this.storedData = parkingSessions;
//	}
//
//	public String getStateName() {
//		return stateName;
//	}
//
//	public void setStateName(String stateName) {
//		this.stateName = stateName;
//	}
//
//	public String getContexPreviewDataLoc() {
//		return contextDataLoc;
//	}

}

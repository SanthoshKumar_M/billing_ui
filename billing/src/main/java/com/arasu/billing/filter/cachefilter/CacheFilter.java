package com.arasu.billing.filter.cachefilter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.arasu.billing.common.UserSession;

public class CacheFilter implements Filter {

	private ArrayList<String> urlList;
	private int totalURLS;
	private static final String LOGINPAGE = "/pages/admin/login/login.xhtml";

	@Override
	public void destroy() {
		
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		String url = request.getServletPath();
		boolean allowedRequest = false;

		for (int i = 0; i < totalURLS; i++) {
			if (url.contains(urlList.get(i))) {
				allowedRequest = true;
				break;
			}
		}
		String contextPath = request.getContextPath();

		if (!allowedRequest) {
			HttpSession session = request.getSession(false);
			if (session == null) {
				response.sendRedirect(contextPath + LOGINPAGE);
			} else {
				UserSession userSession = (UserSession) session.getAttribute("userobject");
				if (userSession == null) {
					response.sendRedirect(contextPath + LOGINPAGE);
				} else {
					try {
						response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
						response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
						response.setDateHeader("Expires", 0);
						chain.doFilter(req, res);
					} catch (Exception e) {
						response.sendRedirect(contextPath + LOGINPAGE);
					}
				}
			}
		} else {

			try {
				response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
				response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
				response.setDateHeader("Expires", 0);
				chain.doFilter(req, res);
			} catch (Exception e) {
				response.sendRedirect(contextPath + LOGINPAGE);
			}
		}

	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		String urls = config.getInitParameter("avoid-urls");
		StringTokenizer token = new StringTokenizer(urls, ",");
		urlList = new ArrayList<>();
		while (token.hasMoreTokens()) {
			urlList.add(token.nextToken());
		}
		totalURLS = urlList.size();
	}

}
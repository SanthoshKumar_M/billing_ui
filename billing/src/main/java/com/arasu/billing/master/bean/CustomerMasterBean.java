package com.arasu.billing.master.bean;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.common.CurrentInstance;
import com.arasu.billing.master.controller.CityMasterController;
import com.arasu.billing.master.to.CustomerMasterTO;

@Named
@ViewScoped
public class CustomerMasterBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private CustomerMasterTO customerMaster = new CustomerMasterTO();
	private CustomerMasterTO selectedCustomerMaster = new CustomerMasterTO();

	private CurrentInstance currentInstance;
	private CityMasterController cityMasterController;

	public CustomerMasterBean() {
		
	}

	@Inject
	public CustomerMasterBean(final CurrentInstance currentInstance, final CityMasterController cityMasterController) {
		this.currentInstance = currentInstance;
		this.cityMasterController = cityMasterController;
	}

	@PostConstruct
	public void init() {
		customerMaster.setCreateDateTime(new Date());
		customerMaster.setCreateUser(currentInstance.getLoginEmployeeDetails());
	}

	public CustomerMasterTO getCustomerMaster() {
		return customerMaster;
	}

	public void setCustomerMaster(CustomerMasterTO customerMaster) {
		this.customerMaster = customerMaster;
	}

	public CurrentInstance getCurrentInstance() {
		return currentInstance;
	}

	public void setCurrentInstance(CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CustomerMasterTO getSelectedCustomerMaster() {
		return selectedCustomerMaster;
	}

	public void setSelectedCustomerMaster(CustomerMasterTO selectedCustomerMaster) {
		this.selectedCustomerMaster = selectedCustomerMaster;
	}

	public CityMasterController getCityMasterController() {
		return cityMasterController;
	}

	public void setCityMasterController(CityMasterController cityMasterController) {
		this.cityMasterController = cityMasterController;
	}

}

package com.arasu.billing.master.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.file.UploadedFile;

import com.arasu.billing.admin.to.LoginTO;
import com.arasu.billing.common.CurrentInstance;
import com.arasu.billing.data.EmployeeData;
import com.arasu.billing.master.controller.EmployeeTypeController;
import com.arasu.billing.master.to.EmployeeMasterTO;
import com.arasu.billing.master.to.EmployeeTypeMasterTO;

@Named
@ViewScoped
public class EmployeeMasterBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private EmployeeData employeeData = new EmployeeData();

	private transient UploadedFile aadhaarPhoto;
	private transient UploadedFile employeePhoto;

	private List<EmployeeTypeMasterTO> employeeTypeTOs = new ArrayList<>();

	private EmployeeTypeController employeeTypeController;

	private EmployeeMasterTO suggestionEmployeeMaster = new EmployeeMasterTO();

	private CurrentInstance currentInstance;

	public EmployeeMasterBean() {
		
	}

	@Inject
	public EmployeeMasterBean(final EmployeeTypeController employeeTypeController, final CurrentInstance currentInstance) {
		this.employeeTypeController = employeeTypeController;
		this.currentInstance = currentInstance;
	}

	@PostConstruct
	public void init() {
		employeeData.setEmployeeMaster(new EmployeeMasterTO());
		employeeData.setLogin(new LoginTO());
		employeeData.getEmployeeMaster().setCreateDateTime(new Date());
		employeeData.getEmployeeMaster().setCreateUser(currentInstance.getLoginUserID());
		employeeTypeTOs = employeeTypeController.getAllEmployeeType();
	}

	public EmployeeData getEmployeeData() {
		return employeeData;
	}

	public void setEmployeeData(EmployeeData employeeData) {
		this.employeeData = employeeData;
	}

	public UploadedFile getAadhaarPhoto() {
		return aadhaarPhoto;
	}

	public void setAadhaarPhoto(UploadedFile aadhaarPhoto) {
		this.aadhaarPhoto = aadhaarPhoto;
	}

	public UploadedFile getEmployeePhoto() {
		return employeePhoto;
	}

	public void setEmployeePhoto(UploadedFile employeePhoto) {
		this.employeePhoto = employeePhoto;
	}

	public List<EmployeeTypeMasterTO> getEmployeeTypeTOs() {
		return employeeTypeTOs;
	}

	public void setEmployeeTypeTOs(List<EmployeeTypeMasterTO> employeeTypeTOs) {
		this.employeeTypeTOs = employeeTypeTOs;
	}

	public EmployeeTypeController getEmployeeTypeController() {
		return employeeTypeController;
	}

	public void setEmployeeTypeController(EmployeeTypeController employeeTypeController) {
		this.employeeTypeController = employeeTypeController;
	}

	public EmployeeMasterTO getSuggestionEmployeeMaster() {
		return suggestionEmployeeMaster;
	}

	public void setSuggestionEmployeeMaster(EmployeeMasterTO suggestionEmployeeMaster) {
		this.suggestionEmployeeMaster = suggestionEmployeeMaster;
	}

	public CurrentInstance getCurrentInstance() {
		return currentInstance;
	}

	public void setCurrentInstance(CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

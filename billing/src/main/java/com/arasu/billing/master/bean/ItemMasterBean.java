package com.arasu.billing.master.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.common.CurrentInstance;
import com.arasu.billing.master.controller.ItemTypeController;
import com.arasu.billing.master.to.ItemMasterTO;
import com.arasu.billing.master.to.ItemTypeMasterTO;

@Named
@ViewScoped
public class ItemMasterBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private ItemMasterTO itemMaster = new ItemMasterTO();
	private ItemMasterTO selectedItemMaster = new ItemMasterTO();
	private List<ItemTypeMasterTO> itemTypeMasterTOs = new ArrayList<>();

	private CurrentInstance currentInstance;
	private ItemTypeController itemTypeController;

	public ItemMasterBean() {
		
	}

	@Inject
	public ItemMasterBean(final CurrentInstance currentInstance, final ItemTypeController itemTypeController) {
		this.currentInstance = currentInstance;
		this.itemTypeController = itemTypeController;
	}

	@PostConstruct
	public void init() {
		itemMaster.setCreateDateTime(new Date());
		itemMaster.setCreateUser(currentInstance.getLoginEmployeeDetails());
		itemTypeMasterTOs = itemTypeController.getAllItemType();

	}

	public ItemMasterTO getItemMaster() {
		return itemMaster;
	}

	public void setItemMaster(ItemMasterTO itemMaster) {
		this.itemMaster = itemMaster;
	}

	public ItemMasterTO getSelectedItemMaster() {
		return selectedItemMaster;
	}

	public void setSelectedItemMaster(ItemMasterTO selectedItemMaster) {
		this.selectedItemMaster = selectedItemMaster;
	}

	public CurrentInstance getCurrentInstance() {
		return currentInstance;
	}

	public void setCurrentInstance(CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<ItemTypeMasterTO> getItemTypeMasterTOs() {
		return itemTypeMasterTOs;
	}

	public void setItemTypeMasterTOs(List<ItemTypeMasterTO> itemTypeMasterTOs) {
		this.itemTypeMasterTOs = itemTypeMasterTOs;
	}

	public ItemTypeController getItemTypeController() {
		return itemTypeController;
	}

	public void setItemTypeController(ItemTypeController itemTypeController) {
		this.itemTypeController = itemTypeController;
	}

}

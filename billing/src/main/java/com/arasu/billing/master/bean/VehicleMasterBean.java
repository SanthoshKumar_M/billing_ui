package com.arasu.billing.master.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.common.CurrentInstance;
import com.arasu.billing.data.VehicleData;
import com.arasu.billing.master.to.VehicleInsuranceDetailsMasterTO;
import com.arasu.billing.master.to.VehicleMasterTO;

@Named
@ViewScoped
public class VehicleMasterBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private VehicleData vehicleData = new VehicleData();
	private CurrentInstance currentInstance;
	private VehicleMasterTO selectVehicleMaster;

	@Inject
	public VehicleMasterBean(final CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	public VehicleMasterBean() {
		
	}

	@PostConstruct
	public void init() {
		vehicleData.getVehicleMaster().setCreateUser(currentInstance.getEmployeeMaster());
	}

	public void addRow() {
		if (vehicleData.getInsuranceDetails().isEmpty() || (null != vehicleData.getInsuranceDetails().get(vehicleData.getInsuranceDetails().size() - 1).getInsuranceNumber()
				&& !vehicleData.getInsuranceDetails().get(vehicleData.getInsuranceDetails().size() - 1).getInsuranceNumber().trim().isEmpty())) {
			vehicleData.getInsuranceDetails().add(new VehicleInsuranceDetailsMasterTO(currentInstance.getEmployeeMaster()));
		}
	}

	public VehicleData getVehicleData() {
		return vehicleData;
	}

	public void setVehicleData(VehicleData vehicleData) {
		this.vehicleData = vehicleData;
	}

	public CurrentInstance getCurrentInstance() {
		return currentInstance;
	}

	public void setCurrentInstance(CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	public VehicleMasterTO getSelectVehicleMaster() {
		return selectVehicleMaster;
	}

	public void setSelectVehicleMaster(VehicleMasterTO selectVehicleMaster) {
		this.selectVehicleMaster = selectVehicleMaster;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}

package com.arasu.billing.master.controller;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.base.controller.BaseController;
import com.arasu.billing.common.Messages;
import com.arasu.billing.master.service.BranchService;
import com.arasu.billing.master.to.BranchMasterTO;

@RequestScoped
@Named
public class BranchController extends BaseController {

	private BranchService branchService;
	private static List<BranchMasterTO> branchSelectItems = new ArrayList<>();

	@Inject
	public BranchController(final Messages messages, final BranchService branchService) {
		super(messages);
		this.branchService = branchService;
	}

	public BranchController() {

	}

	public List<BranchMasterTO> getAllBranchs() {
		return branchService.getAllBranchsList();
	}

	public List<SelectItem> getAllBranchMasterDetails() {
		List<SelectItem> selectItems = new ArrayList<>();
		for (BranchMasterTO branch : getAllBranchs()) {
			selectItems.add(new SelectItem(branch.getId(), branch.getBranchName()));
		}
		return selectItems;
	}

	public List<SelectItem> getAllBranchSelectItems(List<Integer> branchIds) {
		List<SelectItem> selectItems = new ArrayList<>();
		if (branchSelectItems.isEmpty()) {
			branchSelectItems.addAll(getAllBranchs());
		}
		for (BranchMasterTO branch : branchSelectItems) {
			if (!branchIds.contains(branch.getId())) {
				selectItems.add(new SelectItem(branch.getId(), branch.getBranchName()));
			}
		}
		return selectItems;
	}

}

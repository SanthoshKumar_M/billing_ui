package com.arasu.billing.master.controller;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.base.controller.BaseController;
import com.arasu.billing.common.Messages;
import com.arasu.billing.master.service.CityMasterService;
import com.arasu.billing.master.to.CityMasterTO;

@Named
@RequestScoped
public class CityMasterController extends BaseController {
	private CityMasterService cityMasterService;

	@Inject
	public CityMasterController(final CityMasterService cityMasterService, final Messages messages) {
		super(messages);
		this.cityMasterService = cityMasterService;
	}

	public CityMasterController() {
		
	}

	public List<CityMasterTO> getAllCitys() {
		return cityMasterService.getAllCitysList();
	}

	public List<SelectItem> getAllCityMasterDetails() {
		List<SelectItem> selectItems = new ArrayList<>();
		for (CityMasterTO city : getAllCitys()) {
			selectItems.add(new SelectItem(city.getId(), city.getCityName()));
		}
		return selectItems;
	}

	public List<CityMasterTO> citysSuggestionList(String suggestionValue) {
		return cityMasterService.getCitysSuggestionList(suggestionValue);
	}

}

package com.arasu.billing.master.controller;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.base.controller.BaseController;
import com.arasu.billing.common.BillingConstant;
import com.arasu.billing.common.Messages;
import com.arasu.billing.master.bean.CustomerMasterBean;
import com.arasu.billing.master.service.CustomerMasterService;
import com.arasu.billing.master.to.CustomerMasterTO;

@Named
@RequestScoped
public class CustomerMasterController extends BaseController {

	private CustomerMasterBean customerMasterBean;
	private CustomerMasterService customerMasterService;

	@Inject
	public CustomerMasterController(final CustomerMasterBean customerMasterBean, final CustomerMasterService customerMasterService, final Messages messages) {
		super(messages);
		this.customerMasterBean = customerMasterBean;
		this.customerMasterService = customerMasterService;
	}

	public void checkCustomerIsAvailable() {
		isCustomerAvailable();
	}

	private boolean isCustomerAvailable() {
		boolean isCustomerAvailable = customerMasterService.isCustomerAvailable(customerMasterBean.getCustomerMaster().getCustomerName());
		if (isCustomerAvailable && customerMasterBean.getCustomerMaster().getId() == 0) {
			getMessages().displayErrorMessage(null, "error", "Customer Name is Already Available.");
		}
		return isCustomerAvailable;
	}

	public void saveOrUpdateCustomerDetails() {
		int id = customerMasterBean.getCustomerMaster().getId();
		if (0 == id) {
			if (!isCustomerAvailable()) {
				saveOrUpdate(BillingConstant.TYPE_I);
			}
		} else {
			saveOrUpdate(BillingConstant.TYPE_U);
		}

	}

	private void saveOrUpdate(char type) {
		CustomerMasterTO customerMasterTO = customerMasterService.saveOrUpdate(customerMasterBean.getCustomerMaster(), type);
		if (null != customerMasterTO && customerMasterTO.getId() > 0) {
			if (type == BillingConstant.TYPE_I) {
				getMessages().displayInfoMessage(null, "Customer Create Successfully.", "");
				customerMasterService.addCustomerDetails(customerMasterTO);
			} else {
				getMessages().displayInfoMessage(null, "Customer Update Successfully.", "");
				customerMasterService.updateCustomerDetails(customerMasterTO);
			}
			clearCustomerDetails();
			customerMasterBean.init();
		} else {
			getMessages().displayErrorMessage(null, "Customer Not Create Successfully.", "");

		}
	}

	public List<CustomerMasterTO> customerNameSuggestion(String suggestionValue) {
		return customerMasterService.getcustomerList(suggestionValue);
	}

	public List<CustomerMasterTO> getAllCustomerList() {
		return customerMasterService.getAllCustomerDetails();
	}

	public void getCustomerDetailsById() {
		getCustomerDetailsById(customerMasterBean.getSelectedCustomerMaster().getId());
	}

	public void getCustomerDetailsById(int selectedCustomerId) {
		CustomerMasterTO result = customerMasterService.getCustomerDetailsById(selectedCustomerId);
		result.setUpdateUser(customerMasterBean.getCurrentInstance().getLoginEmployeeDetails());
		clearCustomerDetails();
		customerMasterBean.setCustomerMaster(result);
	}

	public void clearCustomerDetails() {
		customerMasterBean.setCustomerMaster(new CustomerMasterTO());
		customerMasterBean.init();
		customerMasterBean.setSelectedCustomerMaster(null);
	}

}

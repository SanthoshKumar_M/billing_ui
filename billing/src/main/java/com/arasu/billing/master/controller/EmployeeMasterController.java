package com.arasu.billing.master.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;

import com.arasu.billing.admin.to.LoginTO;
import com.arasu.billing.base.controller.BaseController;
import com.arasu.billing.common.BillingConstant;
import com.arasu.billing.common.Messages;
import com.arasu.billing.common.sugessionlistbean.EmployeeSuggessionListBean;
import com.arasu.billing.data.EmployeeData;
import com.arasu.billing.master.bean.EmployeeMasterBean;
import com.arasu.billing.master.service.EmployeeMasterService;
import com.arasu.billing.master.to.EmployeeMasterTO;

@Named
@RequestScoped
public class EmployeeMasterController extends BaseController {

	private EmployeeMasterBean employeeMasterBean;
	private EmployeeMasterService employeeMasterService;

	@Inject
	public EmployeeMasterController(final EmployeeMasterBean employeeMasterBean, final EmployeeMasterService employeeMasterService, final Messages messages) {
		super(messages);
		this.employeeMasterBean = employeeMasterBean;
		this.employeeMasterService = employeeMasterService;
	}

	public void handleAadhaarFileUpload(FileUploadEvent event) {
		employeeMasterBean.getEmployeeData().getEmployeeMaster().setAadhaarPhoto(event.getFile().getContent());
	}

	public void handleEmployeeUpload(FileUploadEvent event) {
		employeeMasterBean.getEmployeeData().getEmployeeMaster().setEmployeePhoto(event.getFile().getContent());
	}

	public String getAadhaarImageContentsAsBase64() {
		if (null != employeeMasterBean.getEmployeeData().getEmployeeMaster().getAadhaarPhoto())
			return Base64.getEncoder().encodeToString(employeeMasterBean.getEmployeeData().getEmployeeMaster().getAadhaarPhoto());
		else
			return "";
	}

	public String getEmployeeImageContentsAsBase64() {
		if (null != employeeMasterBean.getEmployeeData().getEmployeeMaster().getEmployeePhoto())
			return Base64.getEncoder().encodeToString(employeeMasterBean.getEmployeeData().getEmployeeMaster().getEmployeePhoto());
		else
			return "";
	}

	public void checkCustomerIsAvailable() {
		isCustomerAvailable();
	}

	private boolean isCustomerAvailable() {
		boolean isCustomerAvailable = employeeMasterService.isCustomerAvailable(employeeMasterBean.getEmployeeData().getEmployeeMaster().getEmployeeName());
		if (isCustomerAvailable && employeeMasterBean.getEmployeeData().getEmployeeMaster().getId() == 0) {
			getMessages().displayErrorMessage(null, "Error", "Customer Name is Already Available.");
		}
		return isCustomerAvailable;
	}

	public void saveOrUpdateEmployeeDetails() {
		int id = employeeMasterBean.getEmployeeData().getEmployeeMaster().getId();
		if (0 == id) {
			if (!isCustomerAvailable()) {
				saveEmployeeDetails(BillingConstant.TYPE_I);
			}
		} else {
			saveEmployeeDetails(BillingConstant.TYPE_U);
		}

	}

	public void saveEmployeeDetails(char type) {
		Map<String, Object> employeeMap = new HashMap<>();
		employeeMap.put("employeeData", employeeMasterBean.getEmployeeData().getEmployeeMaster());
		employeeMap.put("loginData", employeeMasterBean.getEmployeeData().getLogin());
		EmployeeMasterTO employeeMasterTO = employeeMasterService.saveOrUpdate(employeeMap, type);
		if (null != employeeMasterTO && employeeMasterTO.getId() > 0) {
			if (type == BillingConstant.TYPE_I) {
				getMessages().displayInfoMessage(null, "Success", "Employee Create Successfully.");
				EmployeeSuggessionListBean.getEmployeeSuggestionList().add(employeeMasterTO);
			} else {
				getMessages().displayInfoMessage(null, "Success", "Employee Update Successfully.");
			}
			clearEmploeeDetails();
			employeeMasterBean.init();
			employeeMasterService.getAllEmployee();
		} else {
			getMessages().displayErrorMessage(null, "Error", "Employee Not Created.");
		}
	}

	public List<EmployeeMasterTO> employeeNameSuggestion(String suggestionValue) {
		return employeeMasterService.employeeNameSuggestionList(suggestionValue);
	}
	
	public List<EmployeeMasterTO> getAllEmployeeDetails() {
		return employeeMasterService.getAllEmployeeDetails();
	}

	public void getEmployeeDetailsById() {
		getEmployeeDetailsById(employeeMasterBean.getSuggestionEmployeeMaster().getId());
	}
	
	
	public void getEmployeeDetailsById(int selectedEmployeeId) {
		EmployeeData result = employeeMasterService.getEmployeeDetailsById(selectedEmployeeId);
		result.getEmployeeMaster().setUpdateUser(employeeMasterBean.getCurrentInstance().getLoginUserID());
		clearEmploeeDetails();
		employeeMasterBean.getEmployeeData().setEmployeeMaster(result.getEmployeeMaster());
		employeeMasterBean.getEmployeeData().setLogin(result.getLogin());
		employeeMasterBean.getEmployeeData().getEmployeeMaster().setEmployeeType(employeeMasterBean.getEmployeeTypeTOs().stream()
				.filter(emptype -> emptype.getId() == (result.getEmployeeMaster().getEmployeeType().getId())).findFirst().orElse(null));
	}

	public void checkEmployeeType() {
		if (employeeMasterBean.getEmployeeData().getLogin().getId() > 0) {
			if (!employeeMasterBean.getEmployeeData().getEmployeeMaster().getEmployeeType().isLoginAvailable()) {
				employeeMasterBean.getEmployeeData().getLogin().setActiveUser(Boolean.FALSE);
				getMessages().displayErrorMessage(null, "Error", "Employee Login will be Deactivate.");
			} else {
				employeeMasterBean.getEmployeeData().getLogin().setActiveUser(Boolean.TRUE);
			}
		}
	}

	private void clearEmploeeDetails() {
		employeeMasterBean.getEmployeeData().setEmployeeMaster(new EmployeeMasterTO());
		employeeMasterBean.getEmployeeData().setLogin(new LoginTO());
		employeeMasterBean.setAadhaarPhoto(null);
		employeeMasterBean.setEmployeePhoto(null);
		employeeMasterBean.setSuggestionEmployeeMaster(null);

	}
	
	public List<SelectItem> getAllDriver() {
		List<SelectItem> selectItems = new ArrayList<>();
		for (EmployeeMasterTO driver : employeeMasterService.getAllEmployee()) {
			if(driver.getEmployeeType().isLicenseAvailable()) {
				selectItems.add(new SelectItem(driver.getId(), driver.getEmployeeName()));
			}
		}
		return selectItems;
	}
}

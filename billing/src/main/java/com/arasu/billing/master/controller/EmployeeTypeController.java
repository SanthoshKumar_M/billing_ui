package com.arasu.billing.master.controller;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.base.controller.BaseController;
import com.arasu.billing.common.Messages;
import com.arasu.billing.master.service.EmployeeTypeService;
import com.arasu.billing.master.to.EmployeeTypeMasterTO;

@Named
@RequestScoped
public class EmployeeTypeController extends BaseController {
	private EmployeeTypeService employeeTypeService;

	public EmployeeTypeController() {

	}

	@Inject
	public EmployeeTypeController(final EmployeeTypeService employeeTypeService, final Messages messages) {
		super(messages);
		this.employeeTypeService = employeeTypeService;
	}

	public List<EmployeeTypeMasterTO> getAllEmployeeType() {
		return employeeTypeService.getAllEmployeeTypeList();
	}

}

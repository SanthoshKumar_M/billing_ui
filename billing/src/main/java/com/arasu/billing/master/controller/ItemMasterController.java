package com.arasu.billing.master.controller;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.base.controller.BaseController;
import com.arasu.billing.common.BillingConstant;
import com.arasu.billing.common.Messages;
import com.arasu.billing.master.bean.ItemMasterBean;
import com.arasu.billing.master.service.ItemMasterService;
import com.arasu.billing.master.to.ItemMasterTO;

@Named
@RequestScoped
public class ItemMasterController extends BaseController {

	private ItemMasterBean itemMasterBean;
	private ItemMasterService itemMasterService;

	@Inject
	public ItemMasterController(final ItemMasterBean itemMasterBean, final ItemMasterService itemMasterService, final Messages messages) {
		super(messages);
		this.itemMasterBean = itemMasterBean;
		this.itemMasterService = itemMasterService;
	}

	public void checkItemIsAvailable() {
		isItemAvailable();
	}

	private boolean isItemAvailable() {
		boolean isItemAvailable = itemMasterService.isItemAvailable(itemMasterBean.getItemMaster().getItemName());
		if (isItemAvailable && itemMasterBean.getItemMaster().getId() == 0) {
			getMessages().displayErrorMessage(null, "Error", "Item Name is Already Available.");
		}
		return isItemAvailable;
	}

	public void saveOrUpdateItemDetails() {
		int id = itemMasterBean.getItemMaster().getId();
		if (0 == id) {
			if (!isItemAvailable()) {
				saveItemDetails(BillingConstant.TYPE_I);
			}
		} else {
			saveItemDetails(BillingConstant.TYPE_U);
		}

	}

	public void saveItemDetails(char type) {
		ItemMasterTO itemMaster = itemMasterService.saveItemDetails(itemMasterBean.getItemMaster(), type);
		if (null != itemMaster && itemMaster.getId() > 0) {
			if (type == BillingConstant.TYPE_I) {
				getMessages().displayInfoMessage(null, "Success", "Item Create Successfully.");
				itemMasterService.addItemDetails(itemMaster);
			} else {
				getMessages().displayInfoMessage(null, "Success", "Item Update Successfully.");
				itemMasterService.updateItemDetails(itemMaster);
			}
			clearItemDetails();
			itemMasterService.getAllItemDetails();
			itemMasterBean.init();
		} else {
			getMessages().displayErrorMessage(null, "Error", "Item Not Created.");
		}
	}

	public List<ItemMasterTO> itemNameSuggestion(String suggestionValue) {
		return itemMasterService.itemNameSuggestionList(suggestionValue);
	}
	
	public List<ItemMasterTO> getAllItems() {
		return itemMasterService.getAllItems();
	}
	
	public String getItemTypeMasterDetails(int itemTypeMasterId) {
		return itemMasterBean.getItemTypeMasterTOs().stream().filter(emptype -> emptype.getId() == (itemTypeMasterId)).findFirst().orElse(null).getItemTypeName();
	}
	
	public void getItemDetails(int selectedId) {
		getItemDetailsById(selectedId);
	}

	private void getItemDetailsById(int selectedId) {
		ItemMasterTO result = itemMasterService.getItemDetailsById(selectedId);
		result.setUpdateUser(itemMasterBean.getCurrentInstance().getLoginEmployeeDetails());
		clearItemDetails();
		itemMasterBean.setItemMaster(result);
		itemMasterBean.getItemMaster().setItemTypeMaster(
				itemMasterBean.getItemTypeMasterTOs().stream().filter(emptype -> emptype.getId() == (result.getItemTypeMaster().getId())).findFirst().orElse(null));
	}

	public void getItemDetailsById() {
		getItemDetailsById(itemMasterBean.getSelectedItemMaster().getId());
	}

	public void clearItemDetails() {
		itemMasterBean.setItemMaster(new ItemMasterTO());
		itemMasterBean.setSelectedItemMaster(null);
	}

}

package com.arasu.billing.master.controller;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.base.controller.BaseController;
import com.arasu.billing.common.Messages;
import com.arasu.billing.master.service.ItemTypeService;
import com.arasu.billing.master.to.ItemTypeMasterTO;

@Named
@RequestScoped
public class ItemTypeController extends BaseController {
	private ItemTypeService itemTypeService;

	public ItemTypeController() {
		
	}

	@Inject
	public ItemTypeController(final ItemTypeService itemTypeService, final Messages messages) {
		super(messages);
		this.itemTypeService = itemTypeService;
	}

	public List<ItemTypeMasterTO> getAllItemType() {
		return itemTypeService.getAllItemTypeList();
	}

}

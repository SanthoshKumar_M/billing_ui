package com.arasu.billing.master.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.time.DateUtils;

import com.arasu.billing.base.controller.BaseController;
import com.arasu.billing.common.BillingConstant;
import com.arasu.billing.common.Messages;
import com.arasu.billing.common.sugessionlistbean.VehicleSuggessionListBean;
import com.arasu.billing.data.VehicleData;
import com.arasu.billing.master.bean.VehicleMasterBean;
import com.arasu.billing.master.service.VehicleMasterService;
import com.arasu.billing.master.to.VehicleInsuranceDetailsMasterTO;
import com.arasu.billing.master.to.VehicleMasterTO;

@Named
@RequestScoped
public class VehicleMasterController extends BaseController {
	private VehicleMasterBean vehicleMasterBean;
	private VehicleMasterService vehicleMasterService;

	@Inject
	public VehicleMasterController(final VehicleMasterBean vehicleMasterBean, final VehicleMasterService vehicleMasterService, final Messages messages) {
		super(messages);
		this.vehicleMasterBean = vehicleMasterBean;
		this.vehicleMasterService = vehicleMasterService;
	}

	public VehicleMasterController() {
		
	}

	public void checkVehicleIsAvailable() {
		isVehicleAvailable();
	}

	private boolean isVehicleAvailable() {
		boolean isItemAvailable = vehicleMasterService.isVehicleAvailable(vehicleMasterBean.getVehicleData().getVehicleMaster().getVehicleNumber());
		if (isItemAvailable && vehicleMasterBean.getVehicleData().getVehicleMaster().getId() == 0) {
			getMessages().displayErrorMessage(null, "Error", "Vehicle is Already Available.");
		}
		return isItemAvailable;
	}

	public void saveOrUpdateVehicleDetails() {
		int id = vehicleMasterBean.getVehicleData().getVehicleMaster().getId();
		if (0 == id) {
			saveVehicleDetails(BillingConstant.TYPE_I);
		} else {
			saveVehicleDetails(BillingConstant.TYPE_U);
		}

	}

	public void saveVehicleDetails(char type) {

		if (vehicleValidator()) {
			VehicleData vehicleData = vehicleMasterService.saveOrUpdate(vehicleMasterBean.getVehicleData(), type);

			if (null != vehicleData && vehicleData.getVehicleMaster().getId() > 0) {
				if (type == BillingConstant.TYPE_I) {
					getMessages().displayInfoMessage(null, "Success", "Vehicle Master Create Successfully.");
					vehicleMasterService.addVehicleDetails(vehicleData.getVehicleMaster());
				} else {
					getMessages().displayInfoMessage(null, "Success", "Vehicle Master Update Successfully.");
					vehicleMasterService.updateVehicleDetails(vehicleData.getVehicleMaster());
				}
				clear();
				vehicleMasterBean.init();
				VehicleSuggessionListBean.getVehicleMasterTOs().add(vehicleData.getVehicleMaster());
			} else {
				getMessages().displayErrorMessage(null, "Error", "Vehicle Master Not Created.");
			}
		} else {
			getMessages().displayErrorMessage(null, "Error", "Not a Valid Vehicle.");
		}
	}

	public List<VehicleMasterTO> vehicleNumberSuggestion(String suggestionValue) {
		return vehicleMasterService.vehicleNumberSuggestionList(suggestionValue);
	}

	public void getVehicleDetailsById() {
		getVehicleDetailsById(vehicleMasterBean.getSelectVehicleMaster().getId());
	}

	public void getVehicleDetailsById(int selectedId) {
		VehicleData result = vehicleMasterService.getVehicleMasterDetailsById(selectedId);
		result.getVehicleMaster().setUpdateUser(vehicleMasterBean.getCurrentInstance().getEmployeeMaster());
		result.getInsuranceDetails().forEach(vehicleInsurance -> vehicleInsurance.setUpdateUser(vehicleMasterBean.getCurrentInstance().getEmployeeMaster()));
		clear();
		vehicleMasterBean.setVehicleData(result);
	}

	public List<SelectItem> getAllVehicle() {
		List<SelectItem> selectItems = new ArrayList<>();
		for (VehicleMasterTO driver : vehicleMasterService.getAllVehicleDetails()) {
			selectItems.add(new SelectItem(driver.getId(), driver.getVehicleNumber()));
		}
		return selectItems;
	}

	public List<VehicleMasterTO> getAllVehicleDetails() {
		return vehicleMasterService.getAllVehicleDetails();
	}

	public void clear() {
		vehicleMasterBean.setVehicleData(new VehicleData());
		vehicleMasterBean.setSelectVehicleMaster(null);
	}

	private boolean vehicleValidator() {
		return Boolean.TRUE;
	}

	public void getInsuranceToDate(int index) {
		VehicleInsuranceDetailsMasterTO vehicleInsuranceDetailsMasterTO = vehicleMasterBean.getVehicleData().getInsuranceDetails().get(index);
		vehicleInsuranceDetailsMasterTO.setToDate(incrementOneYear(vehicleInsuranceDetailsMasterTO.getFromDate()));
	}

	private Date incrementOneYear(Date fromDate) {
		return DateUtils.addDays(DateUtils.addYears(fromDate, 1), -1);
	}

}

package com.arasu.billing.master.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.base.service.BaseService;
import com.arasu.billing.master.to.BranchMasterTO;
import com.arasu.billing.service.HttpClientService;

@Named
@RequestScoped
public class BranchService extends BaseService {

	public BranchService() {
		
	}

	@Inject
	public BranchService(final HttpClientService httpClientService) {
		super(httpClientService);
	}

	public List<BranchMasterTO> getAllBranchsList() {
		List<BranchMasterTO> branchMasterTOs = new ArrayList<>();
		String url = "getAllBranchDetails";
		BranchMasterTO[] result = getHttpClientService().get(url, BranchMasterTO[].class);
		branchMasterTOs.addAll(Arrays.asList(result));
		return branchMasterTOs;
	}
}

package com.arasu.billing.master.service;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.base.service.BaseService;
import com.arasu.billing.common.sugessionlistbean.CityMasterListBean;
import com.arasu.billing.master.to.CityMasterTO;
import com.arasu.billing.service.HttpClientService;

@Named
@RequestScoped
public class CityMasterService extends BaseService {

	public CityMasterService() {
		
	}

	@Inject
	public CityMasterService(final HttpClientService httpClientService) {
		super(httpClientService);
	}

	public List<CityMasterTO> getAllCitysList() {
		if (CityMasterListBean.getCitySuggestionList().isEmpty()) {
			CityMasterTO[] result = getHttpClientService().get("getAllCityDetails", CityMasterTO[].class);
			CityMasterListBean.setCitySuggestionList(Arrays.asList(result));
		}
		return CityMasterListBean.getCitySuggestionList();
	}

	public List<CityMasterTO> getCitysSuggestionList(String suggestionValue) {
		if (CityMasterListBean.getCitySuggestionList().isEmpty()) {
			getAllCitysList();
		}
		return CityMasterListBean.getCitySuggestionList().stream().filter(city -> city.getCityName().toLowerCase().contains(suggestionValue.toLowerCase())).sorted(Comparator.comparing(CityMasterTO::getCityName))
				.collect(Collectors.toList());
	}
}

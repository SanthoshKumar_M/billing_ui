package com.arasu.billing.master.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;

import com.arasu.billing.base.service.BaseService;
import com.arasu.billing.common.BillingConstant;
import com.arasu.billing.common.CurrentInstance;
import com.arasu.billing.common.sugessionlistbean.CustomerSuggessionListBean;
import com.arasu.billing.master.to.CityMasterTO;
import com.arasu.billing.master.to.CustomerMasterTO;
import com.arasu.billing.service.HttpClientService;

@Named
@RequestScoped
public class CustomerMasterService extends BaseService {
	private CityMasterService cityMasterService;
	private CurrentInstance currentInstance;

	public CustomerMasterService() {

	}

	@Inject
	public CustomerMasterService(final HttpClientService httpClientService, final CityMasterService cityMasterService,
			final CurrentInstance currentInstance) {
		super(httpClientService);
		this.cityMasterService = cityMasterService;
		this.currentInstance = currentInstance;
	}

	public boolean isCustomerAvailable(String customerName) {
		String url = "checkCustomerIsAvailable/" + customerName;
		return getHttpClientService().get(url, Boolean.class);
	}

	public CustomerMasterTO saveOrUpdate(CustomerMasterTO customerMaster, char type) {
		if (type == BillingConstant.TYPE_I) {
			return getHttpClientService().post("saveCustomerDetails", customerMaster, CustomerMasterTO.class);
		} else {
			return getHttpClientService().put("updateCustomerDetails", customerMaster, CustomerMasterTO.class);
		}
	}

	public void getAllCustomerList() {
		if (CustomerSuggessionListBean.getCustomerSuggestionList().isEmpty()) {
			List<CustomerMasterTO> customerMasterTOs;
			String url = "getAllCustomerDetails";
			CustomerMasterTO[] result = getHttpClientService().get(url, CustomerMasterTO[].class);
			customerMasterTOs = Arrays.asList(result);
			CustomerSuggessionListBean.getCustomerSuggestionList().addAll(customerMasterTOs);
		}
	}

	public List<CustomerMasterTO> getcustomerList(String suggestionValue) {
		List<CustomerMasterTO> customerMasterTOs;
		if (CustomerSuggessionListBean.getCustomerSuggestionList().isEmpty()) {
			getAllCustomerList();
			cityMasterService.getAllCitysList();
		}
		customerMasterTOs = filterCustomerDetailsWithSuggession(suggestionValue);
		if (customerMasterTOs.isEmpty()) {
			getNewCustomerList();
			customerMasterTOs = filterCustomerDetailsWithSuggession(suggestionValue);
		}
		return customerMasterTOs;
	}

	private List<CustomerMasterTO> filterCustomerDetailsWithSuggession(String suggestionValue) {
		return CustomerSuggessionListBean.getCustomerSuggestionList().stream()
				.filter(cust -> cust.getCustomerName().toLowerCase().contains(suggestionValue.toLowerCase())
						&& cust.getId() > 0)
				.sorted(Comparator.comparing(CustomerMasterTO::getCustomerName)).collect(Collectors.toList());
	}

	public List<CustomerMasterTO> getAllCustomerDetails() {
		if (CustomerSuggessionListBean.getCustomerSuggestionList().isEmpty()) {
			getAllCustomerList();
		}
		return CustomerSuggessionListBean.getCustomerSuggestionList();
	}

	public CustomerMasterTO getCustomerDetailsById(int id) {
		String url = "getCustomersDetailsById/" + id;
		return getHttpClientService().get(url, CustomerMasterTO.class);
	}

	public void getNewCustomerList() {
		removeCustomerDetails();
		List<Integer> customerIds = CustomerSuggessionListBean.getCustomerSuggestionList().stream()
				.map(CustomerMasterTO::getId).collect(Collectors.toList());
		CustomerMasterTO[] result = getHttpClientService().post("getAllNewCustomerDetails", customerIds,
				CustomerMasterTO[].class);
		CustomerSuggessionListBean.getCustomerSuggestionList().addAll(Arrays.asList(result));
	}

	public void addCustomerDetails(final CustomerMasterTO customerMasterTO) {
		CustomerSuggessionListBean.getCustomerSuggestionList().add(customerMasterTO);
		removeCustomerDetails();
	}

	private void removeCustomerDetails() {
		CustomerSuggessionListBean.getCustomerSuggestionList().removeIf(x -> Arrays.asList(0).contains(x.getId()));
	}

	public void updateCustomerDetails(final CustomerMasterTO customerMasterTO) {
		CustomerMasterTO customer = CustomerSuggessionListBean.getCustomerSuggestionList().stream()
				.filter(customerMaster -> customerMaster.getId() == customerMasterTO.getId()).findAny().orElse(null);
		CustomerSuggessionListBean.getCustomerSuggestionList().remove(customer);
		addCustomerDetails(customerMasterTO);
	}

	public List<CustomerMasterTO> getTranslationCustomerName(String suggestionValue) {
		String suggestion="";
		String lastWord = suggestionValue.substring(suggestionValue.lastIndexOf(" ") + 1);
		if (StringUtils.isNumeric(lastWord)) {
		    suggestion = suggestionValue.substring(0, suggestionValue.lastIndexOf(" "));
		} else {
			lastWord="";
			suggestion=suggestionValue;
		}
		return createCustomList(getHttpClientService().getGetTranslationCustomerNameResponse(suggestion),
				suggestionValue,lastWord);
	}

	private List<CustomerMasterTO> createCustomList(List<Object> list, String suggestionValue, String number) {
		List<CustomerMasterTO> customerMasterTOs = new ArrayList<>();
		if (null != list)
			for (int i = 0; i < list.size(); i++) {
				CustomerMasterTO customerMasterTO = new CustomerMasterTO();
				customerMasterTO.setCustomerName(suggestionValue);
				customerMasterTO.setCustomerPrintName(list.get(i).toString().concat(number==""?"":" "+number));
				customerMasterTO.setMobileNumber("0000000000");
				customerMasterTO.setCustomerAddress(BillingConstant.NEW_CUSTOMER);
				customerMasterTO.setCreateUser(currentInstance.getEmployeeMaster());
				CityMasterTO cityMasterTO = new CityMasterTO();
				cityMasterTO.setCityName("");
				customerMasterTO.setCityMaster(cityMasterTO);
				customerMasterTOs.add(customerMasterTO);
			}
		return customerMasterTOs;
	}
}

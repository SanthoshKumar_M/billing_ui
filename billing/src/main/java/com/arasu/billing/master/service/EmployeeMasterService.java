package com.arasu.billing.master.service;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.base.service.BaseService;
import com.arasu.billing.common.BillingConstant;
import com.arasu.billing.common.sugessionlistbean.EmployeeSuggessionListBean;
import com.arasu.billing.data.EmployeeData;
import com.arasu.billing.master.to.EmployeeMasterTO;
import com.arasu.billing.service.HttpClientService;

@Named
@RequestScoped
public class EmployeeMasterService extends BaseService {

	public EmployeeMasterService() {
		
	}

	@Inject
	public EmployeeMasterService(final HttpClientService httpClientService) {
		super(httpClientService);
	}

	public boolean isCustomerAvailable(String customerName) {
		return getHttpClientService().get("checkEmployeeIsAvailable/" + customerName, Boolean.class);
	}

	public EmployeeMasterTO saveOrUpdate(Map<String, Object> employeeMap, char type) {
		if (type == BillingConstant.TYPE_I) {
			return getHttpClientService().post("saveEmployeeDetails", employeeMap, EmployeeMasterTO.class);
		} else {
			return getHttpClientService().put("updateEmployeeDetails", employeeMap, EmployeeMasterTO.class);
		}
	}

	public List<EmployeeMasterTO> getAllEmployee() {
		if (EmployeeSuggessionListBean.getEmployeeSuggestionList().isEmpty()) {
			String url = "getAllEmployeeDetails";
			EmployeeMasterTO[] result = getHttpClientService().get(url, EmployeeMasterTO[].class);
			EmployeeSuggessionListBean.setEmployeeSuggestionList(Arrays.asList(result));
		}
		return EmployeeSuggessionListBean.getEmployeeSuggestionList();
	}

	public List<EmployeeMasterTO> employeeNameSuggestionList(String suggestionValue) {
		if (EmployeeSuggessionListBean.getEmployeeSuggestionList().isEmpty()) {
			getAllEmployee();
		}
		return EmployeeSuggessionListBean.getEmployeeSuggestionList().stream().filter(emp -> emp.getEmployeeName().toLowerCase().contains(suggestionValue.toLowerCase()))
				.sorted(Comparator.comparing(EmployeeMasterTO::getEmployeeName)).collect(Collectors.toList());
	}

	public List<EmployeeMasterTO> getAllEmployeeDetails() {
		if (EmployeeSuggessionListBean.getEmployeeSuggestionList().isEmpty()) {
			getAllEmployee();
		}
		return EmployeeSuggessionListBean.getEmployeeSuggestionList();
	}

	public EmployeeData getEmployeeDetailsById(int id) {
		return getHttpClientService().get("getEmployeeDetailsById/" + id, EmployeeData.class);
	}
	
	public void addEmployeeDetails(EmployeeMasterTO employeeMasterTO) {
		EmployeeSuggessionListBean.getEmployeeSuggestionList().add(employeeMasterTO);
	}
	
	public void updateEmployeeDetails(EmployeeMasterTO employeeMasterTO) {
		EmployeeMasterTO employee = EmployeeSuggessionListBean.getEmployeeSuggestionList().stream().filter(employeeMaster -> employeeMaster.getId()==employeeMasterTO.getId()).findAny().orElse(null);
		EmployeeSuggessionListBean.getEmployeeSuggestionList().remove(employee);
		addEmployeeDetails(employeeMasterTO);
	}
}

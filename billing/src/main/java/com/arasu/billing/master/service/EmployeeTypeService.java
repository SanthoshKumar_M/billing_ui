package com.arasu.billing.master.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.base.service.BaseService;
import com.arasu.billing.master.to.EmployeeTypeMasterTO;
import com.arasu.billing.service.HttpClientService;

@Named
@RequestScoped
public class EmployeeTypeService extends BaseService {

	public EmployeeTypeService() {
		
	}

	@Inject
	public EmployeeTypeService(final HttpClientService httpClientService) {
		super(httpClientService);
	}

	public List<EmployeeTypeMasterTO> getAllEmployeeTypeList() {
		List<EmployeeTypeMasterTO> employeeTypeTOs = new ArrayList<>();
		String url = "getAllEmployeeType";
		EmployeeTypeMasterTO[] result = getHttpClientService().get(url, EmployeeTypeMasterTO[].class);
		employeeTypeTOs.addAll(Arrays.asList(result));
		return employeeTypeTOs;
	}
}

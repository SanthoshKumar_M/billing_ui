package com.arasu.billing.master.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.base.service.BaseService;
import com.arasu.billing.common.BillingConstant;
import com.arasu.billing.common.sugessionlistbean.ItemSuggessionListBean;
import com.arasu.billing.master.to.ItemMasterTO;
import com.arasu.billing.service.HttpClientService;

@Named
@RequestScoped
public class ItemMasterService extends BaseService {

	public ItemMasterService() {
		
	}

	@Inject
	public ItemMasterService(final HttpClientService httpClientService) {
		super(httpClientService);
	}

	public boolean isItemAvailable(String itemName) {
		return getHttpClientService().get("checkItemNameIsAvailable/" + itemName, Boolean.class);
	}

	public ItemMasterTO saveItemDetails(ItemMasterTO itemMaster, char type) {
		if (type == BillingConstant.TYPE_I) {
			return getHttpClientService().post("saveItemDetails", itemMaster, ItemMasterTO.class);
			
		} else {
			return getHttpClientService().put("updateItemDetails", itemMaster, ItemMasterTO.class);
		}
	}

	public void getAllItemDetails() {
		if (ItemSuggessionListBean.getItemSuggestionList().isEmpty()) {
			List<ItemMasterTO> itemMasterTOs = new ArrayList<>();
			ItemMasterTO[] result = getHttpClientService().get("getAllItemDetails/", ItemMasterTO[].class);
			itemMasterTOs.addAll(Arrays.asList(result));
			ItemSuggessionListBean.setItemSuggestionList(itemMasterTOs);
		}
	}

	public List<ItemMasterTO> itemNameSuggestionList(String suggestionValue) {
		if (ItemSuggessionListBean.getItemSuggestionList().isEmpty()) {
			getAllItemDetails();
		}
		return ItemSuggessionListBean.getItemSuggestionList().stream()
				.filter(item -> item.getItemName().toLowerCase().contains(suggestionValue.toLowerCase())).sorted(Comparator.comparing(ItemMasterTO::getItemName))
				.collect(Collectors.toList());
	}

	public ItemMasterTO getItemDetailsById(int id) {
		return getHttpClientService().get("getItemDetailsById/" + id, ItemMasterTO.class);
	}

	public List<ItemMasterTO> getAllItems() {
		if (ItemSuggessionListBean.getItemSuggestionList().isEmpty()) {
			getAllItemDetails();
		}
		return ItemSuggessionListBean.getItemSuggestionList();
	}
	
	public void addItemDetails(final ItemMasterTO itemMasterTO) {
		ItemSuggessionListBean.getItemSuggestionList().add(itemMasterTO);
	}

	public void updateItemDetails(final ItemMasterTO itemMasterTO) {
		ItemMasterTO item = ItemSuggessionListBean.getItemSuggestionList().stream().filter(itemMaster -> itemMaster.getId()==itemMasterTO.getId()).findAny().orElse(null);
		ItemSuggessionListBean.getItemSuggestionList().remove(item);
		addItemDetails(itemMasterTO);
	}
}

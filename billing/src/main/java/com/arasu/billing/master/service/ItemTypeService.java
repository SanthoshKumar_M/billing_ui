package com.arasu.billing.master.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.base.service.BaseService;
import com.arasu.billing.master.to.ItemTypeMasterTO;
import com.arasu.billing.service.HttpClientService;

@Named
@RequestScoped
public class ItemTypeService extends BaseService {

	public ItemTypeService() {
		
	}

	@Inject
	public ItemTypeService(final HttpClientService httpClientService) {
		super(httpClientService);
	}

	public List<ItemTypeMasterTO> getAllItemTypeList() {
		List<ItemTypeMasterTO> itemTypeMasterTOs = new ArrayList<>();
		String url = "getAllItemTypeDetails";
		ItemTypeMasterTO[] result = getHttpClientService().get(url, ItemTypeMasterTO[].class);
		itemTypeMasterTOs.addAll(Arrays.asList(result));
		return itemTypeMasterTOs;
	}
}

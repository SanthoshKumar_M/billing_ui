package com.arasu.billing.master.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.base.service.BaseService;
import com.arasu.billing.common.BillingConstant;
import com.arasu.billing.common.sugessionlistbean.VehicleSuggessionListBean;
import com.arasu.billing.data.VehicleData;
import com.arasu.billing.master.to.VehicleMasterTO;
import com.arasu.billing.service.HttpClientService;

@Named
@RequestScoped
public class VehicleMasterService extends BaseService {

	public VehicleMasterService() {
		
	}

	@Inject
	public VehicleMasterService(final HttpClientService httpClientService) {
		super(httpClientService);
	}

	public boolean isVehicleAvailable(String vehicleNumber) {
		return getHttpClientService().get("checkVehicleIsAvailable/" + vehicleNumber, Boolean.class);
	}

	public VehicleData saveOrUpdate(VehicleData vehicleData, char type) {
		if (type == BillingConstant.TYPE_I) {
			return getHttpClientService().post("saveVehicleMasterDetails", vehicleData, VehicleData.class);
		} else {
			return getHttpClientService().put("updateVehicleMasterDetails", vehicleData, VehicleData.class);
		}
	}

	public List<VehicleMasterTO> getAllVehicleDetails() {
		if (VehicleSuggessionListBean.getVehicleMasterTOs().isEmpty()) {
			List<VehicleMasterTO> vehicleMasterTOs = new ArrayList<>();
			VehicleMasterTO[] result = getHttpClientService().get("getAllVehicleDetails/", VehicleMasterTO[].class);
			vehicleMasterTOs.addAll(Arrays.asList(result));
			VehicleSuggessionListBean.setVehicleMasterTOs(vehicleMasterTOs);
		}
		return VehicleSuggessionListBean.getVehicleMasterTOs();
	}

	public List<VehicleMasterTO> vehicleNumberSuggestionList(String suggestionValue) {
		if (VehicleSuggessionListBean.getVehicleMasterTOs().isEmpty()) {
			getAllVehicleDetails();
		}
		return VehicleSuggessionListBean.getVehicleMasterTOs().stream()
				.filter(vehicle -> vehicle.getVehicleNumber().toLowerCase().contains(suggestionValue.toLowerCase())).sorted(Comparator.comparing(VehicleMasterTO::getVehicleNumber))
				.collect(Collectors.toList());
	}

	public VehicleData getVehicleMasterDetailsById(int id) {
		return getHttpClientService().get("getVehicleMasterDetailsById/" + id, VehicleData.class);
	}
	
	public void addVehicleDetails(final VehicleMasterTO vehicleMasterTO) {
		VehicleSuggessionListBean.getVehicleMasterTOs().add(vehicleMasterTO);
	}

	public void updateVehicleDetails(final VehicleMasterTO vehicleMasterTO) {
		VehicleMasterTO vehicle = VehicleSuggessionListBean.getVehicleMasterTOs().stream().filter(vehicleMaster -> vehicleMaster.getId()==vehicleMasterTO.getId()).findAny().orElse(null);
		VehicleSuggessionListBean.getVehicleMasterTOs().remove(vehicle);
		addVehicleDetails(vehicleMasterTO);
	}
}

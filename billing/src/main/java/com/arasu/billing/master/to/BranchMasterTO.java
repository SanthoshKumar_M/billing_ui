package com.arasu.billing.master.to;

import java.io.Serializable;

import com.arasu.billing.base.to.BaseTO;

public class BranchMasterTO extends BaseTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String branchName;
	private String branchAddress;
	private int pincode;
	private CityMasterTO cityMaster;
	private String printerName;
	private CompanyMasterTO companyMaster;

	private BranchMasterTO() {
		
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchAddress() {
		return branchAddress;
	}

	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	public CityMasterTO getCityMaster() {
		return cityMaster;
	}

	public void setCityMaster(CityMasterTO cityMaster) {
		this.cityMaster = cityMaster;
	}

	public CompanyMasterTO getCompanyMaster() {
		return companyMaster;
	}

	public void setCompanyMaster(CompanyMasterTO companyMaster) {
		this.companyMaster = companyMaster;
	}

	public String getPrinterName() {
		return printerName;
	}

	public void setPrinterName(String printerName) {
		this.printerName = printerName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

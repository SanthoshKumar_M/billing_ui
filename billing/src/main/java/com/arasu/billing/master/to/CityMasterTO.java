package com.arasu.billing.master.to;

import java.io.Serializable;

import com.arasu.billing.base.to.BaseTO;

public class CityMasterTO extends BaseTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String cityName;
	private StateMasterTO stateMaster;

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public StateMasterTO getStateMaster() {
		return stateMaster;
	}

	public void setStateMaster(StateMasterTO stateMaster) {
		this.stateMaster = stateMaster;
	}

}

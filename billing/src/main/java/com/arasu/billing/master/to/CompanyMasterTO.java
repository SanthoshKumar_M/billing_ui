package com.arasu.billing.master.to;

import java.io.Serializable;

import com.arasu.billing.base.to.BaseTO;

public class CompanyMasterTO extends BaseTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String companyName;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
}

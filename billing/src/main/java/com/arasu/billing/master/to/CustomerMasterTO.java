package com.arasu.billing.master.to;

import java.io.Serializable;

import com.arasu.billing.base.to.BaseTO;
import com.arasu.billing.common.BillingStriingUtils;

public class CustomerMasterTO extends BaseTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String customerName;
	private String customerPrintName;
	private String customerAddress;
	private CityMasterTO cityMaster = new CityMasterTO();
	private String mobileNumber;
	private String alternativeMobileNumber;
	private String phoneNumber;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = BillingStriingUtils.capitalize(customerName);
	}

	public String getCustomerPrintName() {
		return customerPrintName;
	}

	public void setCustomerPrintName(String customerPrintName) {
		this.customerPrintName = customerPrintName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAlternativeMobileNumber() {
		return alternativeMobileNumber;
	}

	public void setAlternativeMobileNumber(String alternativeMobileNumber) {
		this.alternativeMobileNumber = alternativeMobileNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CityMasterTO getCityMaster() {
		return cityMaster;
	}

	public void setCityMaster(CityMasterTO cityMaster) {
		this.cityMaster = cityMaster;
	}

}

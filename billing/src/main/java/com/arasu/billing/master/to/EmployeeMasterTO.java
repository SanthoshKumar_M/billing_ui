package com.arasu.billing.master.to;

import java.io.Serializable;
import java.util.Date;

import com.arasu.billing.common.BillingStriingUtils;

public class EmployeeMasterTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private String employeeName;
	private String employeeAddress;
	private String mobileNumber;
	private Date dateOfJoin;
	private String aadhaarNumber;
	private EmployeeTypeMasterTO employeeType;
	private String licenseNumber;
	private byte[] aadhaarPhoto;
	private byte[] employeePhoto;
	private Date licenseRenewalDate;
	private Integer rowStatus;
	private Integer createUser;
	private Date createDateTime;
	private Integer updateUser;
	private Date updateDateTime;

	public EmployeeMasterTO() {
		this.rowStatus = 1;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = BillingStriingUtils.capitalize(employeeName);
	}

	public String getEmployeeAddress() {
		return employeeAddress;
	}

	public void setEmployeeAddress(String employeeAddress) {
		this.employeeAddress = BillingStriingUtils.capitalize(employeeAddress);
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Date getDateOfJoin() {
		return dateOfJoin;
	}

	public void setDateOfJoin(Date dateOfJoin) {
		this.dateOfJoin = dateOfJoin;
	}

	public String getAadhaarNumber() {
		return aadhaarNumber;
	}

	public void setAadhaarNumber(String aadhaarNumber) {
		this.aadhaarNumber = aadhaarNumber;
	}

	public EmployeeTypeMasterTO getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(EmployeeTypeMasterTO employeeType) {
		this.employeeType = employeeType;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public byte[] getAadhaarPhoto() {
		return aadhaarPhoto;
	}

	public void setAadhaarPhoto(byte[] aadhaarPhoto) {
		this.aadhaarPhoto = aadhaarPhoto;
	}

	public byte[] getEmployeePhoto() {
		return employeePhoto;
	}

	public void setEmployeePhoto(byte[] employeePhoto) {
		this.employeePhoto = employeePhoto;
	}

	public Date getLicenseRenewalDate() {
		return licenseRenewalDate;
	}

	public void setLicenseRenewalDate(Date licenseRenewalDate) {
		this.licenseRenewalDate = licenseRenewalDate;
	}

	public Integer getRowStatus() {
		return rowStatus;
	}

	public void setRowStatus(Integer rowStatus) {
		this.rowStatus = rowStatus;
	}

	public Integer getCreateUser() {
		return createUser;
	}

	public void setCreateUser(Integer createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	public Integer getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(Integer updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

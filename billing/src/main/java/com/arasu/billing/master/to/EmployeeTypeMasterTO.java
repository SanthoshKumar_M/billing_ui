package com.arasu.billing.master.to;

import java.io.Serializable;
import java.util.Date;

public class EmployeeTypeMasterTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private String employeeTypeName;
	private String employeeLevel;
	private boolean loginAvailable;
	private boolean licenseAvailable;
	private int rowStatus;
	private Integer createUser;
	private Date createDateTime;
	private Integer updateUser;
	private Date updateDateTime;

	public EmployeeTypeMasterTO() {
		this.rowStatus = 1;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmployeeTypeName() {
		return employeeTypeName;
	}

	public void setEmployeeTypeName(String employeeTypeName) {
		this.employeeTypeName = employeeTypeName;
	}

	public String getEmployeeLevel() {
		return employeeLevel;
	}

	public void setEmployeeLevel(String employeeLevel) {
		this.employeeLevel = employeeLevel;
	}

	public boolean isLoginAvailable() {
		return loginAvailable;
	}

	public void setLoginAvailable(boolean loginAvailable) {
		this.loginAvailable = loginAvailable;
	}

	public boolean isLicenseAvailable() {
		return licenseAvailable;
	}

	public void setLicenseAvailable(boolean licenseAvailable) {
		this.licenseAvailable = licenseAvailable;
	}

	public Integer getRowStatus() {
		return rowStatus;
	}

	public void setRowStatus(Integer rowStatus) {
		this.rowStatus = rowStatus;
	}

	public Integer getCreateUser() {
		return createUser;
	}

	public void setCreateUser(Integer createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	public Integer getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(Integer updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

package com.arasu.billing.master.to;

import java.io.Serializable;

import com.arasu.billing.base.to.BaseTO;
import com.arasu.billing.common.BillingStriingUtils;

public class ItemMasterTO extends BaseTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String itemName;
	private String itemPrintName;
	private ItemTypeMasterTO itemTypeMaster;

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = BillingStriingUtils.capitalize(itemName);
	}

	public String getItemPrintName() {
		return itemPrintName;
	}

	public void setItemPrintName(String itemPrintName) {
		this.itemPrintName = itemPrintName;
	}

	public ItemTypeMasterTO getItemTypeMaster() {
		return itemTypeMaster;
	}

	public void setItemTypeMaster(ItemTypeMasterTO itemTypeMaster) {
		this.itemTypeMaster = itemTypeMaster;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

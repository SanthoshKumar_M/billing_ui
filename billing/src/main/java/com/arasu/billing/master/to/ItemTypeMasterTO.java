package com.arasu.billing.master.to;

import java.io.Serializable;

import com.arasu.billing.base.to.BaseTO;

public class ItemTypeMasterTO extends BaseTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String itemTypeName;

	public String getItemTypeName() {
		return itemTypeName;
	}

	public void setItemTypeName(String itemTypeName) {
		this.itemTypeName = itemTypeName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

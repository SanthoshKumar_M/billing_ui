package com.arasu.billing.master.to;

import java.io.Serializable;

import com.arasu.billing.base.to.BaseTO;

public class StateMasterTO extends BaseTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String stateName;
	private CountryMasterTO countryMaster;

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public CountryMasterTO getCountryMaster() {
		return countryMaster;
	}

	public void setCountryMaster(CountryMasterTO countryMaster) {
		this.countryMaster = countryMaster;
	}

}

package com.arasu.billing.master.to;

import java.io.Serializable;
import java.util.Date;

import com.arasu.billing.base.to.BaseTO;
import com.arasu.billing.common.BillingConstant;

public class VehicleInsuranceDetailsMasterTO extends BaseTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private VehicleMasterTO vehicleMaster;
	private String insuranceNumber;
	private Date fromDate;
	private Date toDate;
	
	public VehicleInsuranceDetailsMasterTO() {

	}

	public VehicleInsuranceDetailsMasterTO(EmployeeMasterTO createUser) {
		setRowStatus(BillingConstant.ACTIVE);
		setCreateUser(createUser);
	}

	public VehicleMasterTO getVehicleMaster() {
		return vehicleMaster;
	}

	public void setVehicleMaster(VehicleMasterTO vehicleMaster) {
		this.vehicleMaster = vehicleMaster;
	}

	public String getInsuranceNumber() {
		return insuranceNumber;
	}

	public void setInsuranceNumber(String insuranceNumber) {
		this.insuranceNumber = insuranceNumber;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

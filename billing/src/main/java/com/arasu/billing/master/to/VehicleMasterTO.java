package com.arasu.billing.master.to;

import java.io.Serializable;
import java.util.Date;

import com.arasu.billing.base.to.BaseTO;
import com.arasu.billing.common.BillingConstant;
import com.arasu.billing.common.BillingStriingUtils;

public class VehicleMasterTO extends BaseTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String vehicleNumber;
	private int vehicleModel;
	private String vehicleMakeName;
	private String vehicleOwnerName;
	private Date fcDate;
	private Date tax;
	private Date permit;

	public VehicleMasterTO() {
		setRowStatus(BillingConstant.ACTIVE);
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = BillingStriingUtils.capitalize(vehicleNumber);
	}

	public int getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(int vehicleModel) {
		this.vehicleModel = vehicleModel;
	}

	public String getVehicleMakeName() {
		return vehicleMakeName;
	}

	public void setVehicleMakeName(String vehicleMakeName) {
		this.vehicleMakeName = BillingStriingUtils.capitalize(vehicleMakeName);
	}

	public String getVehicleOwnerName() {
		return vehicleOwnerName;
	}

	public void setVehicleOwnerName(String vehicleOwnerName) {
		this.vehicleOwnerName = BillingStriingUtils.capitalize(vehicleOwnerName);
	}

	public Date getFcDate() {
		return fcDate;
	}

	public void setFcDate(Date fcDate) {
		this.fcDate = fcDate;
	}

	public Date getTax() {
		return tax;
	}

	public void setTax(Date tax) {
		this.tax = tax;
	}

	public Date getPermit() {
		return permit;
	}

	public void setPermit(Date permit) {
		this.permit = permit;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

package com.arasu.billing.print;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.PrinterName;

import org.primefaces.util.ComponentUtils;
import org.primefaces.util.Constants;

import com.arasu.billing.common.CurrentInstance;
import com.arasu.billing.common.Messages;
import com.arasu.billing.exception.BillingException;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimplePrintServiceExporterConfiguration;

public class ReportGenerator {
	private static final String DC = "dc";
	private static final String Main = "main";
	private static final String MONTHLY_REPORT = "monthlyreport";
	private static final String MONTHLY_REPORT_NAME = "MonthlyReport.jrxml";

	private static final String REPORT_JRXML = "/reports/jrxml/";

	@Inject
	private Messages messages;

	@Inject
	private CurrentInstance currentInstance;

	public <T> void print(final String fileName, final boolean isDownload, final Map<String, Object> parameters,
			final List<T> data) {
		try {
			JasperPrint jasperPrint = getJasperPrintDC(DC, fileName, parameters, data);
			JasperExportManager.exportReportToPdfFile(jasperPrint, "D:/" + fileName + ".pdf");
			printReportToPrinter(jasperPrint);
			if(Boolean.TRUE.equals(isDownload)) {
				writePDFToResponse(currentInstance.getCurrentInstance().getExternalContext(), jasperPrint, fileName);
			}
		} catch (JRException | IOException e) {
			messages.displayErrorMessage(null, "Error", "Exception in DC Print..!" + e.getMessage());
		}
	}

	private <T> JasperPrint getJasperPrintDC(final String folderName, final String fileName,
			final Map<String, Object> parameters, final List<T> data) throws JRException {
		
		JasperReport mainJasperReport = JasperCompileManager.compileReport(getFilePath(folderName, "main.jrxml"));
		JasperReport jasperReport = JasperCompileManager.compileReport(getFilePath(folderName, fileName));
		parameters.put("SUB_REPORT_PATH", jasperReport);
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(data);
		return JasperFillManager.fillReport(mainJasperReport, parameters, dataSource);
	}

	private String getFilePath(final String folderName, final String fileName) {
		File file = null;
		try {
			URL res = getClass().getClassLoader().getResource(REPORT_JRXML + folderName + File.separator + fileName);
			file = Paths.get(res.toURI()).toFile();
		} catch (URISyntaxException e) {
			throw new BillingException(e.getMessage());
		}
		return file == null ? "" : file.getAbsolutePath();
	}

	private void printReportToPrinter(final JasperPrint jp) throws JRException {
		PrintServiceAttributeSet printServiceAttributeSet = new HashPrintServiceAttributeSet();
		PrintRequestAttributeSet printRequestAttributeSet = new HashPrintRequestAttributeSet();
		printRequestAttributeSet.add(new Copies(1));
		String printerName = currentInstance.getLoginBranchDetails().getPrinterName();

		PrinterName printer = new PrinterName(printerName, null); // gets printer
		printServiceAttributeSet.add(printer);

		JRPrintServiceExporter exporter = new JRPrintServiceExporter();
		SimplePrintServiceExporterConfiguration configuration = new SimplePrintServiceExporterConfiguration();
		configuration.setPrintRequestAttributeSet(printRequestAttributeSet);
		configuration.setPrintServiceAttributeSet(printServiceAttributeSet);
		configuration.setDisplayPageDialog(false);
		configuration.setDisplayPrintDialog(false);

		exporter.setExporterInput(new SimpleExporterInput(jp));
		exporter.setConfiguration(configuration);

		exporter.exportReport();
	}
	
	public <T> void generateMonthlyReport(String fileName, Boolean isDownload, final Map<String, Object> parameters,
			final List<T> data) {
		generateReport(fileName,MONTHLY_REPORT_NAME,isDownload,parameters,data);
	}

	public <T> void generateReport(String fileName,String jrxmlFileName, Boolean isDownload, final Map<String, Object> parameters,
			final List<T> data) {
		try {
			JasperPrint jasperPrint = getJasperPrint(MONTHLY_REPORT, jrxmlFileName, parameters, data);
//			JasperExportManager.exportReportToPdfFile(jasperPrint, "D:/" + fileName + ".pdf");
			if(Boolean.TRUE.equals(isDownload)) {
				downloadFile(jasperPrint,fileName);
			}
		} catch (JRException e) {
			messages.displayErrorMessage(null, "Error", "Exception in DC Print..!" + e.getMessage());
		}
	}
	
	private <T> JasperPrint getJasperPrint(final String folderName, final String fileName,
			final Map<String, Object> parameters, final List<T> data) throws JRException {
		JasperReport jasperReport = JasperCompileManager.compileReport(getFilePath(folderName, fileName));
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(data);
		return JasperFillManager.fillReport(jasperReport, parameters, dataSource);
	}
	
	private void downloadFile(JasperPrint jasperPrint, String fileName) {
		try {
			writePDFToResponse(currentInstance.getCurrentInstance().getExternalContext(), jasperPrint, fileName);
		} catch (JRException | IOException e) {
			e.printStackTrace();
		}
	}

	protected void writePDFToResponse(ExternalContext externalContext, JasperPrint jasperPrint, String fileName)
			throws IOException, JRException {
		 byte[] bytes =JasperExportManager.exportReportToPdf(jasperPrint);
		ByteArrayOutputStream baos = new ByteArrayOutputStream(bytes.length);
		baos.write(bytes, 0, bytes.length);
		externalContext.setResponseContentType("application/pdf");
		externalContext.setResponseHeader("Expires", "0");
		externalContext.setResponseHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		externalContext.setResponseHeader("Pragma", "public");
		externalContext.setResponseHeader("Content-disposition",
				ComponentUtils.createContentDisposition("attachment", fileName + ".pdf"));
		externalContext.setResponseContentLength(baos.size());
		externalContext.addResponseCookie(Constants.DOWNLOAD_COOKIE, "true", Collections.<String, Object>emptyMap());
		OutputStream out = externalContext.getResponseOutputStream();
		baos.writeTo(out);
		externalContext.responseFlushBuffer();
	    FacesContext.getCurrentInstance().responseComplete();
	}
}
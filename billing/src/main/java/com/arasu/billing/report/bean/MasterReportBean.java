package com.arasu.billing.report.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.billing.to.DCListTO;
import com.arasu.billing.common.CurrentInstance;
import com.arasu.billing.master.to.CustomerMasterTO;

@Named
@ViewScoped
public class MasterReportBean implements Serializable {
	
	static final long serialVersionUID = 1L;
	private CurrentInstance currentInstance;

	private Date fromDate;
	private Date toDate;
	
	private CustomerMasterTO customerMaster;
	private int cityMasterId;
	private int driverId;

	private List<DCListTO> dcList = new ArrayList<>();
	
	
	public MasterReportBean() {
		
	}

	@Inject
	public MasterReportBean(final CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	@PostConstruct
	public void init() {
		fromDate = new Date();
		toDate = new Date();
	}

	public CurrentInstance getCurrentInstance() {
		return currentInstance;
	}

	public void setCurrentInstance(CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public CustomerMasterTO getCustomerMaster() {
		return customerMaster;
	}

	public void setCustomerMaster(CustomerMasterTO customerMaster) {
		this.customerMaster = customerMaster;
	}

	public int getCityMasterId() {
		return cityMasterId;
	}

	public void setCityMasterId(int cityMasterId) {
		this.cityMasterId = cityMasterId;
	}

	public int getDriverId() {
		return driverId;
	}

	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}

	public List<DCListTO> getDcList() {
		return dcList;
	}

	public void setDcList(List<DCListTO> dcList) {
		this.dcList = dcList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}

package com.arasu.billing.report.bean;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.common.CurrentInstance;
import com.arasu.billing.master.to.CustomerMasterTO;

@Named
@ViewScoped
public class MonthlyReportBean implements Serializable {
	
	static final long serialVersionUID = 1L;
	private CurrentInstance currentInstance;

	private Date selectedMonthYear;
	private CustomerMasterTO customerMaster;
	
	
	public MonthlyReportBean() {
		
	}

	@Inject
	public MonthlyReportBean(final CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	@PostConstruct
	public void init() {
		selectedMonthYear = new Date();
	}

	public CurrentInstance getCurrentInstance() {
		return currentInstance;
	}

	public void setCurrentInstance(CurrentInstance currentInstance) {
		this.currentInstance = currentInstance;
	}

	public CustomerMasterTO getCustomerMaster() {
		return customerMaster;
	}

	public void setCustomerMaster(CustomerMasterTO customerMaster) {
		this.customerMaster = customerMaster;
	}

	public Date getSelectedMonthYear() {
		return selectedMonthYear;
	}

	public void setSelectedMonthYear(Date selectedMonthYear) {
		this.selectedMonthYear = selectedMonthYear;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}

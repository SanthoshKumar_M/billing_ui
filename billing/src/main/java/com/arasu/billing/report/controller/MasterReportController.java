package com.arasu.billing.report.controller;

import java.sql.Date;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.base.controller.BaseController;
import com.arasu.billing.billing.service.DCService;
import com.arasu.billing.common.Messages;
import com.arasu.billing.master.service.CustomerMasterService;
import com.arasu.billing.master.to.CustomerMasterTO;
import com.arasu.billing.report.bean.MasterReportBean;


@Named
@RequestScoped
public class MasterReportController extends BaseController {

	private MasterReportBean masterReportBean;
	private CustomerMasterService customerMasterService;
	private DCService dcService;

	@Inject
	public MasterReportController(final MasterReportBean masterReportBean,
			final CustomerMasterService customerMasterService, final DCService dcService, final Messages messages) {
		super(messages);
		this.masterReportBean = masterReportBean;
		this.customerMasterService = customerMasterService;
		this.dcService = dcService;
	}

	public List<CustomerMasterTO> customerNameSuggestion(String suggestionValue) {
		return customerMasterService.getcustomerList(suggestionValue);
	}

	public void getDCList() {
		int customerId=null==masterReportBean.getCustomerMaster()?0:masterReportBean.getCustomerMaster().getId();
		masterReportBean.setDcList(dcService.getMasterReport(new Date(masterReportBean.getFromDate().getTime()),
				new Date(masterReportBean.getToDate().getTime()), customerId,
				masterReportBean.getCityMasterId(), masterReportBean.getDriverId()));
	}
}

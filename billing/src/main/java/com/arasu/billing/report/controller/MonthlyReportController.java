package com.arasu.billing.report.controller;

import java.sql.Date;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.PrimeFaces;

import com.arasu.billing.base.controller.BaseController;
import com.arasu.billing.billing.service.DCService;
import com.arasu.billing.billing.to.DCListTO;
import com.arasu.billing.common.DateUtil;
import com.arasu.billing.common.Messages;
import com.arasu.billing.common.PrimeFacesUtil;
import com.arasu.billing.master.service.CustomerMasterService;
import com.arasu.billing.master.to.CustomerMasterTO;
import com.arasu.billing.print.ReportGenerator;
import com.arasu.billing.report.bean.MonthlyReportBean;


@Named
@RequestScoped
public class MonthlyReportController extends BaseController {

	private MonthlyReportBean monthlyReportBean;
	private CustomerMasterService customerMasterService;
	private DCService dcService;
	private ReportGenerator reportGenerator;


	@Inject
	public MonthlyReportController(final MonthlyReportBean monthlyReportBean,
			final CustomerMasterService customerMasterService, final DCService dcService, 
			final ReportGenerator reportGenerator, final Messages messages) {
		super(messages);
		this.monthlyReportBean = monthlyReportBean;
		this.customerMasterService = customerMasterService;
		this.dcService = dcService;
		this.reportGenerator = reportGenerator;
	}

	public List<CustomerMasterTO> customerNameSuggestion(String suggestionValue) {
		return customerMasterService.getcustomerList(suggestionValue);
	}

	public void getDCList() {
		int customerId=null==monthlyReportBean.getCustomerMaster()?0:monthlyReportBean.getCustomerMaster().getId();		
		String fileName=monthlyReportBean.getCustomerMaster().getCustomerName()+" "+DateUtil.getMonthYear(monthlyReportBean.getSelectedMonthYear());
		List<DCListTO> dcList = dcService.getMonthlyReport(new Date(monthlyReportBean.getSelectedMonthYear().getTime()),new Date(DateUtil.getLastDateOfMonth(monthlyReportBean.getSelectedMonthYear()).getTime()),customerId);
		reportGenerator.generateMonthlyReport(fileName,Boolean.TRUE ,null, dcList);
		clear();
	}
	
	private void clear() {
		monthlyReportBean.setSelectedMonthYear(new java.util.Date());
		monthlyReportBean.setCustomerMaster(null);
		monthlyReportBean.getCurrentInstance().getCurrentInstance().getPartialViewContext().getRenderIds().add("monthlyreportcitypanel");
	}


}

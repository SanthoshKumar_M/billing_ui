package com.arasu.billing.scheduler.controller;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.arasu.billing.base.controller.BaseController;
import com.arasu.billing.common.Messages;
import com.arasu.billing.master.service.BranchService;

@RequestScoped
@Named
public class SchedulerController extends BaseController {


	@Inject
	public SchedulerController(final Messages messages, final BranchService branchService) {
		super(messages);
	}

}

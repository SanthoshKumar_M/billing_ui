package com.arasu.billing.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.primefaces.shaded.json.JSONException;

import com.arasu.billing.common.Messages;
import com.arasu.billing.exception.BillingException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Named
@RequestScoped
public class HttpClientService {
	private static final String ERROR = "Error : ";
	private static final String DATA = "Data : ";
	private static final String STATUS = "Status : ";
	private static final String WHITE_SPACE = " ";
	private static final String PERCENTAGE_20 = "%20";
	private static final String UTF_8 = "UTF-8";
	private static final String APPLICATION_JSON_CHARSET_UTF_8 = "application/json;charset=UTF-8";
	private static final String CHARSET = "charset";
	private static final String CONTENT_TYPE = "content-type";
	private String urlPath;

	private static final String HTTP = "http://";
	private static final String CORE = "/billingCore/";

	private static final String BILLING_CORE_IP = "billing.core.ip";
	private static final String BILLING_CORE_PORT = "billing.core.port";

	CloseableHttpClient httpClient = HttpClients.createDefault();
	ObjectMapper mapper = new ObjectMapper();

	@Inject
	private Messages messages;
	private Properties properties = new Properties();

	public HttpClientService() {
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		loadProperties();
		String coreIp = properties.getProperty(BILLING_CORE_IP);
		String corePort = properties.getProperty(BILLING_CORE_PORT);
		urlPath = HTTP.concat(coreIp.concat(":".concat(corePort).concat(CORE)));
	}

	private void loadProperties() {
		try {
			properties.load(getProperties("./properties/config/config.properties"));
		} catch (IOException e) {
			messages.displayErrorMessage("Error", "Error Properties file load.. ", e.getMessage());
		}
	}

	private InputStream getProperties(String fileName) {
		return getClass().getClassLoader().getResourceAsStream(fileName);
	}

	public <T> T get(final String url, final Class<T> classType) {
		T result = null;
		try {
			result = readValueInResponse(getGetResponse(createGetRequest(url)), classType);
		} catch (JSONException | ParseException | BillingException | IOException e) {
			messages.displayErrorMessage(null, "Error in Get", e.getMessage());
		}

		return result;
	}

	private HttpGet createGetRequest(final String url) {
		HttpGet request = new HttpGet(urlPath + url.replace(WHITE_SPACE, PERCENTAGE_20));
		request.addHeader(CONTENT_TYPE, APPLICATION_JSON_CHARSET_UTF_8);
		request.addHeader(CHARSET, UTF_8);
		return request;
	}

	private HttpResponse getGetResponse(HttpGet httpGet) throws IOException {
		return httpClient.execute(httpGet);
	}

	public <T> T post(final String url, final Object value, final Class<T> classType) {
		T result = null;
		try {
			result = readValueInResponse(getPostResponse(createPostRequest(url, value)), classType);
		} catch (JSONException | ParseException | BillingException | IOException e) {
			messages.displayErrorMessage(null, "Error in Save", e.getMessage());
		}

		return result;
	}

	private HttpPost createPostRequest(final String url, final Object value) throws JsonProcessingException {
		HttpPost request = new HttpPost(urlPath + url);
		request.addHeader(CONTENT_TYPE, APPLICATION_JSON_CHARSET_UTF_8);
		request.addHeader(CHARSET, UTF_8);
		String jsonString = mapper.writeValueAsString(value);
		StringEntity params = new StringEntity(jsonString, UTF_8);
		request.setEntity(params);
		return request;
	}

	private HttpResponse getPostResponse(HttpPost httpPost) throws IOException {
		return httpClient.execute(httpPost);
	}

	public <T> T put(final String url, final Object value, final Class<T> classType) {
		T result = null;
		try {
			result = createPutRequest(url, value, classType);
		} catch (JSONException | ParseException | BillingException | IOException e) {
			messages.displayErrorMessage(null, "Error in Update", e.getMessage());
		}

		return result;
	}

	private <T> T createPutRequest(final String url, final Object value, final Class<T> classType) throws IOException {
		HttpPut request = new HttpPut(urlPath + url);
		request.addHeader(CONTENT_TYPE, APPLICATION_JSON_CHARSET_UTF_8);
		request.addHeader(CHARSET, UTF_8);
		String jsonString = mapper.writeValueAsString(value);
		StringEntity params = new StringEntity(jsonString, UTF_8);
		request.setEntity(params);
		return getPutResponse(request, classType);
	}

	private <T> T getPutResponse(HttpPut httpPut, final Class<T> classType) throws IOException {
		HttpResponse response = httpClient.execute(httpPut);
		return readValueInResponse(response, classType);
	}

	private <T> T readValueInResponse(HttpResponse httpResponse, final Class<T> classType)
			throws JSONException, BillingException, ParseException, IOException {
		HttpEntity entity = httpResponse.getEntity();
		APIResponse apiResponse = mapper.readValue(EntityUtils.toString(entity), APIResponse.class);
		if (apiResponse.getStatus() != HttpStatus.SC_OK) {
			throw new BillingException(STATUS + apiResponse.getStatus() + ".<br>" + DATA + apiResponse.getData()
					+ ".<br>" + ERROR + apiResponse.getError() + ".");
		}
		return convertObjectToClass(apiResponse, classType);
	}

	private <T> T convertObjectToClass(APIResponse apiResponse, Class<T> classType) throws JsonProcessingException {
		String jsonString = mapper.writeValueAsString(apiResponse.getData());
		return mapper.readValue(jsonString, classType);
	}

	public List<Object> getGetTranslationCustomerNameResponse(final String suggestionValue) {
		List<Object> objects =new ArrayList<Object>();
		try {
			HttpGet request = new HttpGet("https://inputtools.google.com/request?itc=ta-t-i0-und&num=30&cp=0&cs=1&ie=utf-8&oe=utf-8&app=demopage");
			request.addHeader(CONTENT_TYPE, APPLICATION_JSON_CHARSET_UTF_8);
			request.addHeader(CHARSET, UTF_8);
			 URI uri = new URIBuilder(request.getURI())
				      .addParameter("text", suggestionValue)
				      .build();
				   ((HttpRequestBase) request).setURI(uri);
			objects = new JSONArray(EntityUtils.toString(httpClient.execute(request).getEntity())).getJSONArray(1)
					.getJSONArray(0).getJSONArray(1).toList();
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
			objects.add(suggestionValue);
		}
		return objects;
	}

}
  var autoSaver;
  var txnMode ='insert';
  var form;
  var eventsToListen=["RadioStateChange","input","change","CheckboxStateChange"];
	
		function registerAutoSave(formId,mode){
			  try{
				  if(mode==null||mode==undefined||mode!='insert'){
						return;
				  }
				  	txnMode=mode;
				  	if(mode=='insert'){
				  		addListeners(formId)
					 }
			  }catch(e){
				alert(e);
			  }
		}
		  
		  function checkPrevSession(resumable,formId,mode){
		  try{	
			  if(mode==null||mode==undefined||mode!='insert'){
					return;
			  }
			  	txnMode=mode;
			  	if(mode=='insert'){
			  		addListeners(formId)
				 }
			  
		  if(resumable=='true'){
			  document.getElementById('resumeConfirmation').rf.component.show();
		  }
		  }catch(e){
			alert(e);
		  }
		  }
		  
		  function intializeAutoSave(){
		  	try{
			  	if(txnMode!='insert'){ 
			  	return;
			  	}
			  		
		  		for(var index=0;index<eventsToListen.length;index++){
					form.removeEventListener(eventsToListen[index], intializeAutoSave);
				}
			  autoSaver=setInterval(function(){autoSaveState([{name:'customName', value:'autosave'}]);},10000);
			  console.log(autoSaver);
		  	}catch(e){
				alert(e);
		  	}
		  }
		  
		  function stopAutoSave(){
		  	try{	
		 	 clearInterval(autoSaver);
			form=undefined;
		  	}catch(e){
				alert(e);
		  	}
	
		  }
			function addListeners(formId){
				try{
				if(formId!=null&&formId!=undefined){
				form = document.getElementById(formId);
				for(var index=0;index<eventsToListen.length;index++){
					form.addEventListener(eventsToListen[index]+'',intializeAutoSave);
				}
				}
	
			}catch(e){
				alert(e);
			}
			}
			// autoSave's uptoHere
			
		